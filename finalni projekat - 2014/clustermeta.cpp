﻿#include "clustermeta.h"

ClusterMeta::ClusterMeta(Partition* p, const char c, ClusterNo cn, bool b) : Cluster(p, c, cn, b) {}

Metadata ClusterMeta::get_metadata() {
	std::exit(-1);
}

void ClusterMeta::put_metadata(Metadata) {
	std::exit(-1);
}

void ClusterMeta::get_entry(Entry& e, EntryNum en) {
	std::exit(-1);
}

void ClusterMeta::put_entry(Entry* e, EntryNum en) {
	std::exit(-1);
}

uint32_t ClusterMeta::get_fat_entry(ClusterNo position) {
	if (position >= Helper::fat_entries_in_cluster)
		std::exit(-1);
	auto fat = reinterpret_cast<uint32_t*>(data.data());
	return fat[position];
}

void ClusterMeta::put_fat_entry(uint32_t position, ClusterNo entry) {
	is_dirty = true;
	if (position >= Helper::fat_entries_in_cluster)
		std::exit(-1);
	auto fat = reinterpret_cast<uint32_t*>(data.data());
	fat[position] = entry;
}

void ClusterMeta::initialize_fat(uint32_t& starting_entry, std::vector<ClusterNo>& fat) {
	auto mem_fat = reinterpret_cast<uint32_t*>(data.data());
	auto limit = std::min(Helper::fat_entries_in_cluster, fat.size() - starting_entry); //minimum između maksimuma u klasteru i preostalog broja
	for (uint32_t i = 0; i < limit; i++)
		fat[starting_entry + i] = mem_fat[i];
	starting_entry += Helper::fat_entries_in_cluster;
}

unsigned char* ClusterMeta::get_data() {
	std::exit(-1);
}

void ClusterMeta::put_data(uint32_t position, uint32_t size, std::string & data) {
	std::exit(-1);
}

