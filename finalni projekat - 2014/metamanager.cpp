﻿#include "metamanager.h"

MetaManager::MetaManager(SpecificFilesystem *file_system): fs(*file_system), metadata(0, 0, 0, 0) {
	Cluster *c = nullptr;
	meta_clusters.push_back(c = fs.clusters->get_cluster(0, true, false)); //prvi klaster
	while (meta_clusters.size() != fs.number_of_meta_clusters)
		meta_clusters.push_back(fs.clusters->get_cluster(meta_clusters.size(), true, false)); //broj klastera je simultano redni broj u kolekciji prvih N klastera
	get_meta_values_from_cluster();

	if (metadata.value0x04) {
		uint32_t current = 0;
		fat.assign(fs.number_of_all_clusters, 0); //fat tabela keširana u memoriji - fiksna veličina N
		for (uint32_t i = 0; i < meta_clusters.size(); i++)
			meta_clusters[i]->initialize_fat(current, fat);
		fat_first = fat_last = metadata.value0x00; fat_count = 0;
		while (fat[fat_last]) {
			++fat_count;
			fat_last = fat[fat_last];
		}
	}
}

MetaManager::~MetaManager() {
	set_meta_values_to_cluster();
	uint32_t cluster_number = 0;
	uint32_t i = 0;
	uint32_t fat_backup_remaining = fat.size();
	uint32_t first_cluster_limit = std::min(fat_backup_remaining, Helper::fat_entries_in_first_cluster);
	for (i = 0; i < first_cluster_limit; i++)
		meta_clusters[cluster_number]->put_fat_entry(i, fat[i]); //bekap prvog klastera
	++cluster_number;
	uint32_t fat_count = 0;
	for (i = first_cluster_limit; i < fat_backup_remaining; i++) {
		meta_clusters[cluster_number]->put_fat_entry(fat_count, fat[i]);
		if ((++fat_count) == Helper::fat_entries_in_cluster) {
			++cluster_number;
			fat_count = 0;
		}
	}
	for (auto c : meta_clusters)
		fs.clusters->return_cluster(c);
	meta_clusters.clear();
}

void MetaManager::format() {
	for (auto c : meta_clusters)
		fs.clusters->return_cluster_no_writeback(c); //false -> ne upisujemo fs nazad na particiju (nebitni podaci)
	meta_clusters.clear();

	Cluster *c = nullptr;
	meta_clusters.push_back(c = fs.clusters->get_cluster(0, false, false)); //prvi klaster
	while (meta_clusters.size() != fs.number_of_meta_clusters)
		meta_clusters.push_back(fs.clusters->get_cluster(meta_clusters.size(), false, false)); //broj klastera je simultano redni broj u kolekciji prvih N klastera

	fat.assign(fs.number_of_all_clusters, 0);
	metadata = {
		fs.number_of_meta_clusters, //početak liste slobodnih klastera
		fs.number_of_all_clusters - fs.number_of_meta_clusters, //koliko zauzima FAT
		0, //početak sadržaja korenog direktorijuma (lazy alokacija?)
		0 //koliko ima ulaza u korenom direktorijumu
	};
	fat_first = fs.number_of_meta_clusters; fat_last = fs.number_of_all_clusters - 1; fat_count = fat_last - fat_first + 1;
	for (uint32_t i = fat_first; i <= fat_last; i++)
		fat[i] = i + 1;
	fat.back() = 0;
}

ClusterNo MetaManager::allocate_next(ClusterNo previous) {
	if (previous)
		fat[previous] = fat_first;
	ClusterNo current = fat_first;
	metadata.value0x00 = fat_first = fat[fat_first];
	fat[current] = 0;
	if (!fat_first)
		metadata.value0x00 = fat_last = 0;
	--fat_count;
	return current;
}

std::list<ClusterNo> MetaManager::allocate_next(ClusterNo previous, uint32_t number) {
	std::list<ClusterNo> clusters;
	for (uint32_t i = 0; i < number; i++) {
		clusters.push_back(allocate_next(previous));
		previous = clusters.back();
	}
	return clusters;
}

void MetaManager::free_cluster(ClusterNo entry) {
	++fat_count;
	if (fat_last) { //ako je 
		fat[fat_last] = entry;
		fat_last = entry;
		fat[fat_last] = 0;
	}
	else {
		fat_last = fat_first = entry;
		fat[fat_first] = 0;
	}
}

void MetaManager::free_clusters(std::vector<ClusterNo> numbers) {
	for (auto n : numbers)
		free_cluster(n);
}

uint32_t MetaManager::get_count_of_free_clusters() const {
	return fat_count;
}

bool MetaManager::clusters_available() const {
	return fat_count > 0;
}

bool MetaManager::clusters_available(uint32_t count) const {
	return fat_count >= count;
}

void MetaManager::set_meta_values(Metadata m) {
	metadata = m;
}

const Metadata MetaManager::get_meta_values() const {
	return metadata;
}

void MetaManager::set_meta_values_to_cluster() {
	meta_clusters[0]->put_metadata(metadata);
	meta_clusters[0]->write_to_partition();
}

void MetaManager::get_meta_values_from_cluster() {
	metadata = meta_clusters[0]->get_metadata();
}

void MetaManager::initialize_fat(uint32_t cluster) {
	uint32_t entries = (cluster * ClusterSize < metadata.value0x04) ? metadata.value0x04 % ClusterSize : ClusterSize;
	for (uint32_t i = 0; i < entries; i++)
		fat[cluster*ClusterSize + i] = meta_clusters[cluster]->get_fat_entry(i);
}

ClusterNo MetaManager::get_cluster_number(ClusterNo entry) {
	return fat[entry];
}
std::list<ClusterNo> MetaManager::get_cluster_numbers(ClusterNo entry, uint32_t number) {
	std::list<ClusterNo> entries;
	for (uint32_t i = 0; i < number; i++) {
		entries.push_back(entry);
		entry = fat[entry];
	}
	return entries;
}