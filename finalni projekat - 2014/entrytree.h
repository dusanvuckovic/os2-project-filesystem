﻿#pragma once

#include <cstdint>
#include <map>
#include <string>
#include <list>
#include <vector>

using ClusterNo = unsigned long;
class Cluster;
class EntryTree {
protected:
	friend class EntryManager;

	uint32_t first_cluster;
	uint32_t size;
	EntryTree* parent;
	std::string name;
	std::vector<Cluster*> clusters; //klasteri za podatke u slučaju fajla ili ulaze u slučaju direktorijuma
public:
	EntryTree(std::string name);
	EntryTree(std::string name, EntryTree* parent);
	EntryTree(std::string name, EntryTree* parent, uint32_t first_cluster, uint32_t size);
	virtual ~EntryTree() = default;
	EntryTree(EntryTree&) = delete;
	EntryTree(EntryTree&&) = delete;

	void update_parent();

	uint32_t get_size() const;
	ClusterNo get_first_cluster() const;
	std::string get_name() const;

	uint32_t get_current_cluster_size() const;
	ClusterNo get_last_cluster_number() const;
	virtual uint32_t get_needed_cluster_size() const = 0;
	

	void add_another_cluster(Cluster *c);
	Cluster* remove_last_cluster();
	void set_clusters(std::vector<Cluster*> clusters);
	void set_clusters(std::list<Cluster*> clusters);
	
	virtual std::list<EntryTree*> get_entries() = 0;
	virtual void add_new_file(std::string name) = 0;
	virtual void add_new_directory(std::string name) = 0;
	virtual EntryTree* get_entry(std::string& s) = 0;
	virtual void remove_entry(std::string& s) = 0;
	virtual bool is_directory() const = 0;
};
