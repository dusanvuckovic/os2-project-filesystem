#pragma once

#include "cluster.h"
#include "fs.h"

class Cluster;
struct Entry;
using EntryNum = unsigned long;
class ClusterDirectory final: public Cluster {
public:
	ClusterDirectory(Partition*, const char, ClusterNo, bool);
	~ClusterDirectory() = default;

	Metadata get_metadata() override;
	void put_metadata(Metadata) override;
	void get_entry(Entry&, EntryNum) override;
	void put_entry(Entry*, EntryNum) override;
	uint32_t get_fat_entry(ClusterNo) override;
	void put_fat_entry(uint32_t, ClusterNo) override;
	void initialize_fat(uint32_t& starting_entry, std::vector<ClusterNo>& fat) override;
	unsigned char* get_data() override;
	void put_data(uint32_t position, uint32_t size, std::string& data) override;
};

