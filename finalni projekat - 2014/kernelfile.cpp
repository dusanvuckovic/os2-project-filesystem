﻿#include "kernelfile.h"

KernelFile::KernelFile(SpecificFilesystem* fs, std::string filepath, bool is_write_enabled, bool pre_existing, EntryFile* entry): 
is_writeable(is_write_enabled), filepath(filepath), fs(*fs), cursor(0), file_size(entry->size), first_cluster(entry->first_cluster), file_clusters(entry->clusters),
file(*entry) {
	if (pre_existing && is_write_enabled)
		cursor = file_size;
}

KernelFile::~KernelFile() {
	fs.files->close_file(filepath);
	if (is_writeable) {
		file.update_parent();
		fs.files->end_write(filepath);
	}
	else
		fs.files->end_read(filepath);
}

bool KernelFile::is_there_space(BytesCnt b) const {
	return cursor + b < cluster_file_size();
}

uint32_t KernelFile::number_of_new_clusters_needed(BytesCnt b) {
	uint32_t number_of_new_clusters = 0, number_of_current_clusters = file_clusters.size();
	do
		++number_of_new_clusters;
	while ((cursor + b) >= cluster_file_size(number_of_new_clusters));
	return number_of_new_clusters;
}

BytesCnt KernelFile::remaining_in_cluster() const {
	return ClusterSize - (cursor % ClusterSize);
}

BytesCnt KernelFile::cursor_cluster() const {
	return cursor / ClusterSize;
}

BytesCnt KernelFile::cursor_bytes() const {
	return cursor % ClusterSize;
}

BytesCnt KernelFile::cluster_file_size() const {
	return ClusterSize * file_clusters.size();
}

BytesCnt KernelFile::cluster_file_size(uint32_t new_clust) const {
	return ClusterSize * (file_clusters.size() + new_clust);
}

BytesCnt KernelFile::remaining_in_file() const {
	return file_size - cursor;
}

bool KernelFile::are_there_clusters(ClusterNo b) {
	return fs.metadata->clusters_available(number_of_new_clusters_needed(b));
}

void KernelFile::allocate_clusters(BytesCnt b) {
	auto number = number_of_new_clusters_needed(b);
	ClusterNo last_cluster = 0;
	if (file_clusters.size())
		last_cluster = file_clusters.back()->get_cluster_number();
	auto cluster_number_list = fs.metadata->allocate_next(last_cluster, number);
	auto cluster_list = fs.clusters->get_multiple_clusters(cluster_number_list, false, false);
	file_clusters.insert(file_clusters.end(), cluster_list.begin(), cluster_list.end()); //dodaj na kraj vektora svih klastera
	first_cluster = file_clusters[0]->get_cluster_number();
}

void KernelFile::copy_to_clusters(BytesCnt write_remaining, std::string& data) {
	uint32_t bytes_to_write;
	if (cursor_bytes()) { //ako je prvi klaster delimično popunjen
		bytes_to_write = std::min(remaining_in_cluster(), write_remaining);
		file_clusters[cursor / ClusterSize]->put_data(cursor % ClusterSize, bytes_to_write, data); //prekopiraj do njegovog kraja
		write_remaining -= bytes_to_write;
		cursor += bytes_to_write;
	}
	while (write_remaining) {
		bytes_to_write = std::min(ClusterSize, write_remaining);
		file_clusters[cursor_cluster()]->put_data(0, bytes_to_write, data); //popuni klaster (ceo ili deo)
		write_remaining -= bytes_to_write;
		cursor += bytes_to_write;
	}
}

char KernelFile::write(BytesCnt bytes, char *buffer) {
	if (!is_writeable || !bytes)
		return 0;
	if (!is_there_space(bytes)) {
		if (!are_there_clusters(bytes))
			return 0;
		else
			allocate_clusters(bytes);
	}
	std::string write_data(buffer, bytes);
	file_size += bytes;
	copy_to_clusters(bytes, write_data); //pomera i cursor
	for (auto f : file_clusters)
		f->write_to_partition();
	return 1;
}

BytesCnt KernelFile::read(BytesCnt bytes_to_read, char* buffer) {
	if (!bytes_to_read || file_size == cursor)
		return 0;
	BytesCnt offset = 0; //takođe i offset
	BytesCnt next_cluster_size = std::min(remaining_in_cluster(), bytes_to_read); //prostor preostao u prvom klasteru
	next_cluster_size = std::min(next_cluster_size, remaining_in_file()); //ako ima manje za čitanje nego što je zadato
	if (cursor_bytes()) {
		std::memcpy(buffer, file_clusters[cursor_cluster()]->get_data() + cursor_bytes(), next_cluster_size);
		cursor += next_cluster_size;
		offset += next_cluster_size;
		bytes_to_read -= next_cluster_size;
	}
	while (bytes_to_read && file_size != cursor) {
		next_cluster_size = std::min(ClusterSize, bytes_to_read);
		next_cluster_size = std::min(next_cluster_size, remaining_in_file());
		std::memcpy(buffer + offset, file_clusters[cursor_cluster()]->get_data(), next_cluster_size);
		cursor += next_cluster_size;
		offset += next_cluster_size;
		bytes_to_read -= next_cluster_size;
	}
	return offset;
}

char KernelFile::seek(BytesCnt b) {
	if (b >= file_size)
		return 0;
	cursor = b;
	return 1;
}

BytesCnt KernelFile::file_position() {
	return cursor;
}

char KernelFile::eof() {
	if (cursor == file_size)
		return 1;
	return 0;
}

BytesCnt KernelFile::get_file_size() {
	return file_size;
}

char KernelFile::truncate() {
	if (!file_size)
		return 0;
	if (!cursor) {
		file_size = cursor = first_cluster = 0;
		for (auto c : file_clusters)
			fs.clusters->return_cluster(c);
		return 1;
	}
	uint32_t last_valid_cluster = cursor_cluster();
	uint32_t total_cluster_number = file_clusters.size();
	for (uint32_t i = last_valid_cluster + 1; i < total_cluster_number; i++)
		fs.clusters->return_cluster(file_clusters[i]);
	file_clusters.resize(last_valid_cluster + 1); //ostaju prvih n klastera obuhvacena kursorom
	file_size = cursor;
	return 1;
}