#pragma once

#include <cstdint>
#include <vector>
#include <iostream>

struct Metadata {
	uint32_t value0x00;
	uint32_t value0x04;
	uint32_t value0x08;
	uint32_t value0x0c;

	friend std::ostream& operator << (std::ostream& os, const Metadata& dt) {
		return os << dt.value0x00 << dt.value0x04 << dt.value0x08 << dt.value0x0c;
	}

	Metadata(uint32_t, uint32_t, uint32_t, uint32_t);
	~Metadata() = default;
};

