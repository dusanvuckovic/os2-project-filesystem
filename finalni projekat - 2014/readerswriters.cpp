#include "readerswriters.h"

ReadersWriters::ReadersWriters(): internal_mutex(), file_block(), global_counter(0), current_counter(0), read_count(0) {

}

void ReadersWriters::start_read() {
	std::unique_lock<std::mutex> my_lock(internal_mutex);
	uint32_t specific_counter = global_counter++;
	while (current_counter != specific_counter) {
		my_lock.unlock();
		file_block.wait();
		my_lock.lock();
	}
	++read_count;
	++current_counter;
	file_block.signal_all();
}

void ReadersWriters::start_write() {
	std::unique_lock<std::mutex> my_lock(internal_mutex);
	uint32_t specific_counter = global_counter++;
	while ((specific_counter != current_counter) || read_count) {
		my_lock.unlock();
		file_block.wait();
		my_lock.lock();
	}
}

void ReadersWriters::end_read() {
	std::lock_guard<std::mutex> my_lock(internal_mutex);
	if (!(--read_count))
		file_block.signal();
}

void ReadersWriters::end_write() {
	std::lock_guard<std::mutex> my_lock(internal_mutex);
	++current_counter;
	file_block.signal_all();
}
