#pragma once

#include <cstdint>
#include <string>
#include "part.h"
#include "fs.h"

struct Entry;
class Helper {
public:
	Helper() = delete;
	~Helper() = delete;
	
	static uint32_t calculate_number_of_clusters(uint32_t, uint32_t);
	static uint32_t calculate_number_of_meta_clusters(uint32_t);
	static void fill_entry_file(Entry& e, std::string& s, uint32_t first_cluster = 0, uint32_t size = 0);
	static void fill_entry_directory(Entry& e, std::string s, uint32_t first_cluster = 0, uint32_t size = 0);
	static std::string get_string_from_entry(Entry *e);
	static uint32_t read_unsigned(const char*);

	static const uint32_t fat_entries_in_cluster = ClusterSize / 4;
	static const uint32_t fat_entries_in_first_cluster = (ClusterSize - 16) / 4;
	static const uint32_t entry_entries_in_cluster = ClusterSize / 20;
};

