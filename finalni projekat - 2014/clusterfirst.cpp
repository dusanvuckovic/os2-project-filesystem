﻿#include "clusterfirst.h"

ClusterFirst::ClusterFirst(Partition* p, const char c, ClusterNo cn, bool b) : Cluster(p, c, cn, b) {}

Metadata ClusterFirst::get_metadata() {
	auto meta = reinterpret_cast<uint32_t*>(data.data());
	Metadata m = {meta[0], meta[1], meta[2], meta[3]};
	return m;
}

void ClusterFirst::put_metadata(Metadata m) {
	is_dirty = true;
	uint32_t *meta = reinterpret_cast<uint32_t*>(data.data());
	meta[0] = m.value0x00; meta[1] = m.value0x04; meta[2] = m.value0x08; meta[3] = m.value0x0c;
}
void ClusterFirst::get_entry(Entry& e, EntryNum en) {
	std::exit(-1);
}
void ClusterFirst::put_entry(Entry* e, EntryNum en) {
	std::exit(-1);
}
uint32_t ClusterFirst::get_fat_entry(ClusterNo position) {
	if (position >= Helper::fat_entries_in_first_cluster)
		std::exit(-1);
	auto fat = reinterpret_cast<uint32_t*>(data.data() + 16);
	return fat[position];
}
void ClusterFirst::put_fat_entry(uint32_t position, ClusterNo entry) {
	is_dirty = true;
	if (position >= Helper::fat_entries_in_first_cluster)
		std::exit(-1);
	auto fat = reinterpret_cast<uint32_t*>(data.data() + 16);
	fat[position] = entry;	
}

void ClusterFirst::initialize_fat(uint32_t& starting_entry, std::vector<ClusterNo>& fat) {
	auto mem_fat = reinterpret_cast<uint32_t*>(data.data() + 16);
	auto limit = std::min(Helper::fat_entries_in_first_cluster, fat.size()); //minimum između maksimuma u klasteru i preostalog broja
	for (uint32_t i = 0; i < limit; i++)
		fat[starting_entry + i] = mem_fat[i];
	mem_fat[limit] = 0;
	starting_entry += Helper::fat_entries_in_first_cluster;
}

unsigned char* ClusterFirst::get_data() {
	std::exit(-1);
}

void ClusterFirst::put_data(uint32_t position, uint32_t size, std::string & data) {
	std::exit(-1);
}
