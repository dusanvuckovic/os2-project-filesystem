#pragma once
#include <map>
#include <mutex>
#include "conditionvariablefifo.h"

class ReadersWriters {

private:
	uint32_t global_counter;
	uint32_t current_counter;

	uint32_t read_count;
	std::mutex internal_mutex;
	ConditionVariableFIFOC11 file_block;
public:

	void start_read();
	void start_write();
	void end_read();
	void end_write();
	
	ReadersWriters();
	~ReadersWriters() = default;
	ReadersWriters(const ReadersWriters&) = delete;
	ReadersWriters& operator = (ReadersWriters&) = delete;
};

