﻿#include "specfs.h"

SpecificFilesystem::SpecificFilesystem(const char partition_label, Partition* partition_object): partition_label(partition_label),
partition_object(partition_object), partition_mutex(), open_mutex() { //mount
	number_of_all_clusters = partition_object->getNumOfClusters();
	number_of_meta_clusters = Helper::calculate_number_of_meta_clusters(number_of_all_clusters);

	clusters = new ClusterManager(this);
	metadata = new MetaManager(this);
	entries = new EntryManager(this);
	files = new FileManager(this);
}

SpecificFilesystem::~SpecificFilesystem() { //unmount
	delete entries;
	delete metadata;
	delete files;
	delete clusters;
}

void SpecificFilesystem::format() {
	metadata->format();
	entries->format();
	files->format();
}

bool SpecificFilesystem::does_exist(std::string& s) {
	return entries->does_exist(s);
}

void SpecificFilesystem::open_file_read(File* f, KernelFile*& kf, std::string filepath) {
	files->start_read(filepath);
	kf = new KernelFile(this, filepath, false, true, entries->get_entry_file(filepath)); //napravi novi
	files->open_new_file(filepath); //dodaj u listu otvorenih
}
void SpecificFilesystem::open_file_append(File* f, KernelFile*& kf, std::string filepath) {
	files->start_write(filepath);
	kf = new KernelFile(this, filepath, true, true, entries->get_entry_file(filepath)); //napravi novi
	files->open_new_file(filepath); //dodaj u listu otvorenih
}

void SpecificFilesystem::open_file_write(File* f, KernelFile*& kf, std::string filepath) {
	if (entries->does_exist(filepath) && !files->is_file_open(filepath)) //ako fajl postoji i zatvoren je
		delete_file(filepath); //obriši ga
	else if (!entries->check_parent_entry_availability(filepath)) { //a ako pravimo novi fajl i nema mesta, ništa
		files->abort_file_creation(f); return;
	}
	files->start_write(filepath);
	entries->add_new_file(filepath);
	kf = new KernelFile(this, filepath, true, false, entries->get_entry_file(filepath)); //napravi novi i dodaj u drvo direktorijuma
	files->open_new_file(filepath); //dodaj u listu otvorenih
}

char SpecificFilesystem::delete_file(std::string& s) {
	if (!does_exist(s) || files->is_file_open(s))
		return 0; //fajl ne postoji ili fajl otvoren, ne može se izbrisati
	entries->remove_entry(s);
	return 1;
}

char SpecificFilesystem::create_directory(std::string& s) {
	if (does_exist(s) || !entries->check_parent_entry_availability(s)) //direktorijum već postoji ili nema mesta u njegovom roditelju
		return 0;
	entries->add_new_directory(s);
	return 1;
}

char SpecificFilesystem::delete_directory(std::string& s) {
	if (!does_exist(s) || !entries->get_entry(s)->is_directory() || entries->get_entry(s)->get_size()) //ako ulaz u FS ne postoji, nije direktorijum ili nije prazan
		return 0; //ne može da se obriše
	entries->remove_entry(s);
	return 1;
}

char SpecificFilesystem::read_directory(std::string& s, EntryNum number, Entry& e) {
	if (!does_exist(s) || !entries->get_entry(s)->is_directory())
		return 0; //ulaz ne postoji ili nije direktorijum
	auto entry = entries->get_entry_directory(s);
	if (number > entry->get_size())
		return 2; //prekoračenje
	entry->read_entry(e, number);
	return 1;
}
uint32_t SpecificFilesystem::number_of_open_files() const {
	return files->number_of_open_files();
}
void SpecificFilesystem::stop_file_creation() {
	files->stop_file_creation();
}
bool SpecificFilesystem::file_creation_allowed() const {
	return files->can_create_files();
}
//interface

void SpecificFilesystem::block() {
	synchro.wait();
}

void SpecificFilesystem::unblock() {
	synchro.signal();
}

std::string SpecificFilesystem::get_root_name() const {
	std::string root_name;
	root_name += partition_label;
	root_name += R"(:\)";
	return root_name;
}