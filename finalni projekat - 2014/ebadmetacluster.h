#pragma once

#include <exception>
#include <iostream>
#include "fs.h"

using ClusterNo = unsigned long;

class ExceptionBadMetadataCluster: public std::exception {
private:
	ClusterNo c;
	const char p;
public:
	ExceptionBadMetadataCluster(ClusterNo c, const char p): c(c), p(p) {
		std::cout << what();
	}
	~ExceptionBadMetadataCluster() = default;
	const char* what() const {
		std::string s("Error in the partition ");
		s += std::to_string(p);
		s += " where cluster ";
		s += std::to_string(c);
		s += "tried to access metadata";
		return s.c_str();
	}
};

