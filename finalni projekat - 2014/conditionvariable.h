#pragma once

#include <cstdint>

class ConditionVariable {
public:
	ConditionVariable() = default;
	virtual ~ConditionVariable() = default;

	virtual void wait() = 0; //blocks a thread
	virtual void signal() = 0; //unblocks a thread
	virtual void signal_all() = 0; //unblocks all threads
	
	virtual bool is_empty() const = 0;
	virtual uint32_t size() const = 0;

};