#include "filemanager.h"

void FileManager::abort_file_creation(File* f) {
	delete f;
	f = nullptr;
}

FileManager::FileManager(SpecificFilesystem *file_system): fs(*file_system) {
	file_creation_allowed = true;
}

FileManager::~FileManager() {
	for (auto& m : synchro_files)
		delete m.second;
}

void FileManager::format() {
	file_creation_allowed = true;
}

uint32_t FileManager::number_of_open_files() const {
	return opened_files.size();
}

bool FileManager::is_file_open(std::string& filename) const {
	if (opened_files.count(filename))
		return true;
	return false;
}

void FileManager::stop_file_creation() {
	file_creation_allowed = false;
}

bool FileManager::can_create_files() const {
	return file_creation_allowed;
}

void FileManager::open_new_file(std::string filename) {
	if (is_file_open(filename))
		++opened_files[filename];
	else
		opened_files[filename] = 1;

}
void FileManager::close_file(std::string filename) {
	if (!(--opened_files[filename])) {
		opened_files.erase(filename);
		if (opened_files.size())
			fs.unblock();
	}
}

void FileManager::start_read(std::string filepath) {
	if (!synchro_files.count(filepath))
		synchro_files[filepath] = new ReadersWriters();
	synchro_files[filepath]->start_read();
}
void FileManager::start_write(std::string filepath) {
	if (!synchro_files.count(filepath))
		synchro_files[filepath] = new ReadersWriters();
	synchro_files[filepath]->start_write();
}
void FileManager::end_read(std::string filepath) {
	synchro_files[filepath]->end_read();
}
void FileManager::end_write(std::string filepath) {
	synchro_files[filepath]->end_write();
}