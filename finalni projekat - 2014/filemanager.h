#pragma once
#include "file.h"
#include "specfs.h"
#include "kernelfile.h"
#include "readerswriters.h"

class File;
class KernelFile;
class FileManager {
private:

	SpecificFilesystem& fs;
	
	bool file_creation_allowed;
	std::map<std::string, uint32_t> opened_files;
	std::map<std::string, ReadersWriters*> synchro_files;
	
public:
	FileManager(SpecificFilesystem*);
	~FileManager();
	FileManager(const FileManager&) = delete;
	FileManager& operator = (const FileManager&) = delete;

	void format();
	
	uint32_t number_of_open_files() const;
	bool is_file_open(std::string&) const;

	void stop_file_creation();
	bool can_create_files() const;

	void abort_file_creation(File *f);

	void open_new_file(std::string filename);
	void close_file(std::string filename);

	void start_read(std::string);
	void start_write(std::string);
	void end_read(std::string);
	void end_write(std::string);
};