#pragma once
#include "entrytree.h"
#include <list>
#include <vector>

class Cluster;
class EntryTree;
class EntryFile: public EntryTree {
	friend class KernelFile;
public:
	EntryFile(std::string name);
	EntryFile(std::string name, EntryTree* parent_entry);
	EntryFile(std::string name, EntryTree* parent, uint32_t first_cluster, uint32_t size);
	~EntryFile();

	inline uint32_t get_needed_cluster_size() const override;
	std::list<EntryTree*> get_entries() override;
	void add_new_file(std::string name) override;
	void add_new_directory(std::string name) override;
	EntryTree* get_entry(std::string& s) override;
	void remove_entry(std::string& s) override;
	inline bool is_directory() const override;
};

