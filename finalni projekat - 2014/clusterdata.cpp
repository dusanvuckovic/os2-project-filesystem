#include "clusterdata.h"

ClusterData::ClusterData(Partition* p, const char c, ClusterNo cn, bool b): Cluster(p, c, cn, b) {}

Metadata ClusterData::get_metadata() {
	exit(-1);
}

void ClusterData::put_metadata(Metadata m) {
	std::exit(-1);
}

void ClusterData::get_entry(Entry& e, EntryNum en) {
	std::exit(-1);
}

void ClusterData::put_entry(Entry* e, EntryNum en) {
	std::exit(-1);
}

uint32_t ClusterData::get_fat_entry(ClusterNo cn) {
	std::exit(-1);
}

void ClusterData::put_fat_entry(uint32_t ui, ClusterNo cn) {
	std::exit(-1);
}

void ClusterData::initialize_fat(uint32_t& starting_entry, std::vector<ClusterNo>& fat) {
	std::exit(-1);
}

unsigned char* ClusterData::get_data() {
	return data.data();
}

void ClusterData::put_data(uint32_t position, uint32_t size, std::string& data) {
	is_dirty = true;
	std::memcpy(this->data.data() + position, data.data(), size);
	data = data.substr(size);
}
