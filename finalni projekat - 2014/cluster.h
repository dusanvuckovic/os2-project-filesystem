#pragma once

#include <array>
#include <vector>
#include <cstdint>
#include "metadata.h"
#include "part.h"

struct Entry;
using EntryNum = unsigned long;
using BytesCnt = unsigned long;
using ClusterNo = unsigned long;
class ClusterManager;
class Partition;
struct Metadata;
class Cluster {
friend class ClusterManager;
protected:
	Cluster(Partition*, const char, ClusterNo, bool); //bool -> read data from partition when constructing OR initialize with zeroes
	~Cluster();
	std::array<unsigned char, ClusterSize> data;
	const char partition_label;
	Partition* partition_object;
	const ClusterNo cluster_number;
	bool is_dirty;
public:
	void write_to_partition(); //iff necessary
	void unconditional_write_to_partition();
	ClusterNo get_cluster_number() const;

	virtual Metadata get_metadata() = 0; //baca gre�ku ako nije nulti klaster
	virtual void put_metadata(Metadata) = 0; //baca gre�ku ako nije nulti klaster
	virtual void get_entry(Entry&, EntryNum) = 0;
	virtual void put_entry(Entry*, EntryNum) = 0;
	virtual uint32_t get_fat_entry(ClusterNo) = 0;
	virtual void put_fat_entry(uint32_t, ClusterNo) = 0;
	virtual void initialize_fat(uint32_t& starting_entry, std::vector<ClusterNo>& fat) = 0;
	virtual unsigned char* get_data() = 0;
	virtual void put_data(uint32_t position, uint32_t size, std::string& data) = 0;
};

