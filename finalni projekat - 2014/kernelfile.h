#pragma once

#include "file.h"
#include "cluster.h"
#include "part.h"
#include "fs.h"
#include "specfs.h"
#include "entrytree.h"
#include "entryfile.h"
#include "readerswriters.h"

using BytesCnt = unsigned long;
using ClusterNo = unsigned long;
using Cursor = BytesCnt;
class Cluster;
class File;
class SpecificFilesystem;
class EntryFile;
class KernelFile {
private:
	std::string filepath;
	bool is_writeable;
	Cursor cursor;
	uint32_t& file_size;
	uint32_t& first_cluster;
	std::vector<Cluster*>& file_clusters;
	SpecificFilesystem& fs;
	EntryFile& file;

	uint32_t KernelFile::number_of_new_clusters_needed(BytesCnt);
	BytesCnt remaining_in_cluster() const;
	BytesCnt cursor_cluster() const;
	BytesCnt cursor_bytes() const;
	BytesCnt cluster_file_size() const;
	BytesCnt cluster_file_size(uint32_t) const;
	BytesCnt remaining_in_file() const;
	bool is_there_space(BytesCnt) const;
	bool are_there_clusters(ClusterNo);
	void allocate_clusters(BytesCnt);
	void copy_to_clusters(BytesCnt, std::string&);
public:

	char write(BytesCnt, char* buffer);
	BytesCnt read(BytesCnt, char* buffer);
	char seek(BytesCnt);
	BytesCnt file_position();
	char eof();
	BytesCnt get_file_size();
	char truncate();

	KernelFile(SpecificFilesystem*, std::string, bool, bool, EntryFile*); //bools: write flag, preexisting
	~KernelFile();

	void start_read();
	void start_write();
	void end_read();
	void end_write();

};

