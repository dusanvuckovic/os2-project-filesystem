#include "entrytree.h"
#include "helper.h"

EntryTree::EntryTree(std::string name) : name(name), first_cluster(0), size(0), parent(nullptr) {}
EntryTree::EntryTree(std::string name, EntryTree* parent): name(name), first_cluster(0), size(0), parent(parent) {}
EntryTree::EntryTree(std::string name, EntryTree* parent, uint32_t first_cluster, uint32_t size): name(name), parent(parent), first_cluster(first_cluster), size(size) {}

uint32_t EntryTree::get_size() const {
	return size;
}

ClusterNo EntryTree::get_first_cluster() const {
	return first_cluster;
}

std::string EntryTree::get_name() const {
	return name;
}

void EntryTree::set_clusters(std::vector<Cluster*> vector_clusters) {
	if (clusters.size())
		std::cout << "Error: clusters getting overwritten!";
	clusters = vector_clusters;
	first_cluster = clusters.front()->get_cluster_number();
}

void EntryTree::set_clusters(std::list<Cluster*> list_clusters) {
	if (clusters.size())
		std::cout << "Error: clusters getting overwritten!";
	clusters = {list_clusters.begin(), list_clusters.end()};
	first_cluster = clusters.front()->get_cluster_number();
}

uint32_t EntryTree::get_current_cluster_size() const {
	return clusters.size();
}

ClusterNo EntryTree::get_last_cluster_number() const {
	if (clusters.size())
		return clusters.back()->get_cluster_number();
	return 0;
}


void EntryTree::add_another_cluster(Cluster *c) {
	if (!clusters.size())
		first_cluster = c->get_cluster_number();
	clusters.push_back(c);
}
Cluster* EntryTree::remove_last_cluster() {
	if (!clusters.size())
		return nullptr;
	else if (clusters.size() == 1)
		first_cluster = 0;
	auto last = clusters.back();
	clusters.pop_back();
	return last;
}

void EntryTree::update_parent() {
	if (!parent)
		return;
	reinterpret_cast<EntryDirectory*>(parent)->update_entry(this);
}