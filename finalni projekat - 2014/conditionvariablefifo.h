#pragma once
#include "conditionvariable.h"
#include <mutex>
#include <memory>
#include <condition_variable>
#include <list>
class ConditionVariableFIFOC11: public ConditionVariable {

private:
	std::mutex blocker_lock;
	std::list<std::condition_variable*> blocker_variables;
public:
	void wait();
	void signal();
	void signal_all();

	bool is_empty() const;
	uint32_t size() const;
	ConditionVariableFIFOC11() = default;
	~ConditionVariableFIFOC11() = default;
};

