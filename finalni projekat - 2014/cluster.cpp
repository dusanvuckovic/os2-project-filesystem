#include "cluster.h"

Cluster::Cluster(Partition* partition_object, const char partition_label, ClusterNo cluster_number, bool load_existing):
partition_object(partition_object), partition_label(partition_label), cluster_number(cluster_number), is_dirty(false) {
	if (load_existing)
		partition_object->readCluster(cluster_number, reinterpret_cast<char*>(data.data()));
	else
		data.fill(0);
}

Cluster::~Cluster() {
	if (is_dirty) 
		partition_object->writeCluster(cluster_number, reinterpret_cast<char*>(data.data()));
}

void Cluster::write_to_partition() {
	if (is_dirty) {
		is_dirty = false;
		partition_object->writeCluster(cluster_number, reinterpret_cast<char*>(data.data()));
	}
}

void Cluster::unconditional_write_to_partition() {
	partition_object->writeCluster(cluster_number, reinterpret_cast<char*>(data.data()));
	is_dirty = false;
}

ClusterNo Cluster::get_cluster_number() const {
	return cluster_number;
}
