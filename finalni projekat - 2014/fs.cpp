#include "fs.h"

KernelFS* FS::myImpl = KernelFS::get_instance();

FS::FS() {
	if (!myImpl)
		myImpl = KernelFS::get_instance();
}

FS::~FS() {
	if (myImpl)
		delete myImpl;
}
char FS::mount(Partition* partition) {
	return myImpl->mount(partition);
}
char FS::unmount(char part) {
	return myImpl->unmount(part);
}
char FS::format(char part) {
	return myImpl->format(part);
}
char FS::doesExist(char* fname) {
	return myImpl->does_exist(fname);
}
File* FS::open(char* fname, char mode) {
	return myImpl->open(fname, mode);
}
char FS::deleteFile(char* fname) {
	return myImpl->delete_file(fname);
}
char FS::createDir(char* dirname) {
	return myImpl->create_directory(dirname);
}
char FS::deleteDir(char* dirname) {
	return myImpl->delete_directory(dirname);
}
char FS::readDir(char* dirname, EntryNum n, Entry &e) {
	return myImpl->read_directory(dirname, n, e);
}

