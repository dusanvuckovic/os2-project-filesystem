#include "helper.h"

uint32_t Helper::calculate_number_of_clusters(uint32_t number_of_entries, uint32_t entries_per_cluster) {
	return (!(number_of_entries % entries_per_cluster)) ? number_of_entries / entries_per_cluster : (number_of_entries / entries_per_cluster) + 1;
}

uint32_t Helper::calculate_number_of_meta_clusters(uint32_t total_cluster_entries) {
	uint32_t num_of_clusters = 1;
	if (total_cluster_entries > fat_entries_in_first_cluster) {
		total_cluster_entries -= fat_entries_in_first_cluster; //koliko je ostalo
		num_of_clusters += calculate_number_of_clusters(total_cluster_entries, fat_entries_in_cluster);
	}
	return num_of_clusters;
}

uint32_t Helper::read_unsigned(const char* my_data) {
	return my_data[0] | my_data[1] << 8 | my_data[2] << 16 | my_data[3] << 24;
}

void Helper::fill_entry_file(Entry& e, std::string& s, uint32_t first_cluster, uint32_t size) { //first_cluster, size default to zero
	auto delimiter = s.find(".");
	auto filename = s.substr(0, delimiter);
	auto extension = s.substr(delimiter + 1);
	while (filename.size() != FNAMELEN)
		filename += " ";
	while (extension.size() != FEXTLEN)
		extension += " ";
	e.attributes = 1;
	std::memcpy(e.name, filename.data(), FNAMELEN);
	std::memcpy(e.ext, extension.data(), FEXTLEN);
	e.firstCluster = first_cluster;
	e.size = size;
	return;
}

void Helper::fill_entry_directory(Entry& e, std::string filename, uint32_t first_cluster, uint32_t size) { //first_cluster, size default to zero
	while (filename.size() != FNAMELEN)
		filename += " ";
	e.attributes = 2;
	std::memcpy(e.name, filename.data(), FEXTLEN);
	std::memcpy(e.ext, "   ", FEXTLEN);
	e.firstCluster = first_cluster;
	e.size = size;
	return;
}

std::string Helper::get_string_from_entry(Entry *e) {
	std::string hold = e->name;
	hold.erase(FNAMELEN);
	if (e->attributes == 1) {
		hold += '.';
		hold += e->ext;
		hold.erase(FNAMELEN+FEXTLEN+1);
		std::string helper;
		for (auto c : hold)
			if (c != ' ')
				helper.push_back(c);
		hold = helper;
	}
	else
		hold.erase(hold.find(' '));
	return hold;
}
