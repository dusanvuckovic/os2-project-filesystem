#pragma once

#include <vector>
#include "metadata.h"
#include "specfs.h"

struct Metadata;
class SpecificFilesystem;
class Cluster;
using ClusterNo = unsigned long;
class MetaManager {
public:
	MetaManager(SpecificFilesystem*);
	~MetaManager();

	SpecificFilesystem& fs;

	Metadata metadata;
	std::vector<Cluster*> meta_clusters;

	void format();
	
	void set_meta_values(Metadata);
	const Metadata get_meta_values() const;
	void set_meta_values_to_cluster();
	void get_meta_values_from_cluster();
	
	void initialize_fat(uint32_t cluster);

	std::vector<ClusterNo> fat;
	ClusterNo fat_first, fat_last, fat_count;

	bool clusters_available() const;
	bool clusters_available(uint32_t) const;
	ClusterNo allocate_next(ClusterNo previous);
	std::list<ClusterNo> allocate_next(ClusterNo previous, uint32_t number);
	void free_cluster(ClusterNo to_be_freed);
	void free_clusters(std::vector<ClusterNo> to_be_freed);
	uint32_t get_count_of_free_clusters() const;

	ClusterNo get_cluster_number(ClusterNo entry);
	std::list<ClusterNo> get_cluster_numbers(ClusterNo entry, uint32_t number);
};

