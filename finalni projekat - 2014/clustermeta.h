#pragma once

#include "cluster.h"
#include "kernelfs.h"

class Cluster;
class Partition;
struct Metadata;
class ClusterMeta final: public Cluster {
public:
	ClusterMeta(Partition*, const char, ClusterNo, bool);
	~ClusterMeta() = default;

	Metadata get_metadata() override;
	void put_metadata(Metadata) override;
	void get_entry(Entry&, EntryNum) override;
	void put_entry(Entry*, EntryNum) override;
	uint32_t get_fat_entry(ClusterNo) override;
	void put_fat_entry(uint32_t, ClusterNo) override;
	void initialize_fat(uint32_t& starting_entry, std::vector<ClusterNo>& fat) override;
	unsigned char* get_data() override;
	void put_data(uint32_t position, uint32_t size, std::string& data) override;
};

