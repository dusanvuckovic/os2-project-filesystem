#include "clusterdirectory.h"

ClusterDirectory::ClusterDirectory(Partition* p, const char c, ClusterNo cn, bool b) : Cluster(p, c, cn, b) {}

Metadata ClusterDirectory::get_metadata() {
	exit(-1);
}

void ClusterDirectory::put_metadata(Metadata) {
	exit(-1);
}

void ClusterDirectory::get_entry(Entry& e, EntryNum n) {
	auto entries = reinterpret_cast<Entry*>(data.data());
	e = entries[n];
}

void ClusterDirectory::put_entry(Entry* e, EntryNum n) {
	is_dirty = true;
	auto entries = reinterpret_cast<Entry*>(data.data());
	entries[n] = *e;
}

uint32_t ClusterDirectory::get_fat_entry(ClusterNo cn) {
	std::exit(-1);
}

void ClusterDirectory::put_fat_entry(uint32_t position, ClusterNo entry) {
	std::exit(-1);
}

void ClusterDirectory::initialize_fat(uint32_t& starting_entry, std::vector<ClusterNo>& fat) {
	std::exit(-1);
}

unsigned char* ClusterDirectory::get_data() {
	std::exit(-1);
}

void ClusterDirectory::put_data(uint32_t position, uint32_t size, std::string & data) {
	std::exit(-1);
}