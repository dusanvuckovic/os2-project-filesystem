#include "entryfile.h"
#include "entrytree.h"
#include <vector>
#include "cluster.h"
#include "helper.h"


EntryFile::EntryFile(std::string name) : EntryTree(name) {}
EntryFile::EntryFile(std::string name, EntryTree* parent_entry) : EntryTree(name, parent_entry) {}
EntryFile::EntryFile(std::string name, EntryTree* parent, uint32_t first_cluster, uint32_t size) : EntryTree(name, parent, first_cluster, size) {}

std::list<EntryTree*> EntryFile::get_entries() {
	exit(-1);
}

uint32_t EntryFile::get_needed_cluster_size() const {
	return Helper::calculate_number_of_clusters(size, ClusterSize);
}

void EntryFile::add_new_file(std::string name) {
	exit(-1);
}

void EntryFile::add_new_directory(std::string name) {
	exit(-1);
}

EntryTree* EntryFile::get_entry(std::string& s) {
	exit(-1);
}

void EntryFile::remove_entry(std::string& s) {
	exit(-1);
}

bool EntryFile::is_directory() const {
	return false;
}

EntryFile::~EntryFile() = default;