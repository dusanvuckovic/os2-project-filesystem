#pragma once

#include <string>
#include <map>
#include <set>
#include <memory>
#include <mutex>
#include <cctype>
#include <condition_variable>
#include "cluster.h"
#include "file.h"
#include "part.h"
#include "specfs.h"
#include "conditionvariablefifo.h"
#include "helper.h"

using EntryNum = unsigned long;

class SpecificFilesystem;
class Partition;
class File;
struct Entry;
class KernelFS {
private:
	KernelFS();
	
	static KernelFS* singleton_fs;
	std::set<char> free_partitions;
	std::map<char, SpecificFilesystem*> specific_filesystems;
	std::map<char, std::condition_variable> partition_blockers;
	std::mutex partition_mutex;

	bool is_valid_name(const std::string&) const;
public:
	static KernelFS* get_instance();
	~KernelFS();
	char mount(Partition*);
	char unmount(const char);
	char format(const char);
	char does_exist(std::string);
	File* open(std::string, char);
	char delete_file(std::string);
	char create_directory(std::string);
	char delete_directory(std::string);
	char read_directory(std::string, EntryNum, Entry&); //prvim argumentom se zadaje apsolutna putanja, drugim redni broj ulaza koji se cita, treci argument je adresa na kojoj se smesta procitani ulaz
};

