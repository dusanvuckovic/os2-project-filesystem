﻿#include "entrymanager.h"

EntryManager::EntryManager(SpecificFilesystem *filesystem): fs(*filesystem) {
	auto meta = fs.metadata->get_meta_values();
	if (meta.value0x04) {
		root = new EntryDirectory(fs.get_root_name(), nullptr, meta.value0x08, meta.value0x0c);
		reconstruct_fs();
	}
	else
		root = new EntryDirectory(fs.get_root_name(), nullptr, 0, 0);
}

EntryManager::~EntryManager() {
	update_metadata();
	clear_entries();
}

void EntryManager::format() {
	clear_entries();
	root = new EntryDirectory(fs.get_root_name(), nullptr, 0, 0);
}

void EntryManager::clear_entries() {
	if (!root)
		return;
	std::list<EntryTree*> entries = {root};
	while (entries.size()) {
		EntryTree* current = entries.front();
		entries.pop_front();
		if (current->is_directory())
			for (auto i: current->get_entries())
				entries.push_back(i);
		while (auto clust = current->remove_last_cluster())
			fs.clusters->return_cluster(clust);
		delete current;
	}
}

void EntryManager::reconstruct_fs() {
	std::list<EntryTree*> directories = {root};
	std::list<ClusterNo> clusters_list;
	while (directories.size()) {
		EntryDirectory* current = reinterpret_cast<EntryDirectory*>(directories.front());
		directories.pop_front();
		clusters_list = fs.metadata->get_cluster_numbers(current->get_first_cluster(), current->get_needed_cluster_size());
		current->set_clusters(fs.clusters->get_multiple_clusters(clusters_list, true, true));
		current->branch_down();
		for (auto entry : current->get_entries())
			if (entry->is_directory())
				directories.push_front(entry);	
			else {
				clusters_list = fs.metadata->get_cluster_numbers(entry->get_first_cluster(), entry->get_needed_cluster_size());
				entry->set_clusters(fs.clusters->get_multiple_clusters(clusters_list, true, false));
			}
	}
}

std::vector<std::string> EntryManager::split_filenames(std::string name) {
	std::vector<std::string> vector;
	name = name.substr(3); /*removes LTTR:\ */
	std::string hold;
	for (auto c : name) {
		if (c != '\\') //ako nije beksleš
			hold.push_back(c); //stavi u string
		else {
			vector.push_back(hold);
			hold.clear();
		}
	}
	if (hold != "")
		vector.push_back(hold);
	return vector;
}

void EntryManager::update_metadata() const {
	fs.metadata->metadata.value0x08 = root->get_first_cluster();
	fs.metadata->metadata.value0x0c = root->get_size();
	fs.metadata->set_meta_values_to_cluster();
}

bool EntryManager::check_parent_entry_availability(std::string& s) {
	auto entry = get_entry_parent(s);
	if (entry->get_size() != Helper::entry_entries_in_cluster)
		return true;
	return false;
}

void EntryManager::add_new_file(std::string s) {
	auto parent = get_entry_parent(s);
	s = s.substr(s.find_last_of('\\') + 1); //skraćuje string na ime fajla
	if (parent->is_full())
		parent->add_another_cluster(fs.clusters->get_cluster(fs.metadata->allocate_next(parent->get_last_cluster_number()), true, true));
	parent->add_new_file(s);
	if (!parent->parent) //ako je koreni
		update_metadata();
}

void EntryManager::add_new_directory(std::string s) {
	auto parent = get_entry_parent(s);
	s = s.substr(s.find_last_of('\\')); //skraćuje string na ime fajla
	if (parent->is_full())
		parent->add_another_cluster(fs.clusters->get_cluster(fs.metadata->allocate_next(parent->get_last_cluster_number()), true, true));
	parent->add_new_directory(s);
	if (!parent->parent) //ako je koreni
		update_metadata();
}

void EntryManager::remove_entry(std::string s) {
	auto names = split_filenames(s);
	auto entry = get_entry(s);
	auto parent = get_entry_parent(s);
	for (auto c : entry->clusters) {
		fs.metadata->free_cluster(c->get_cluster_number());
		fs.clusters->return_cluster(c);
	}
	entry->clusters.clear();
	parent->remove_entry(names.back());
	if (!(parent->size % Helper::entry_entries_in_cluster)) { //ako je zauzeće pun klaster
		Cluster *c = parent->remove_last_cluster();
		fs.metadata->free_cluster(c->get_cluster_number());
		fs.clusters->return_cluster_no_writeback(c); //nema relevantnih podataka u klasteru, ne moramo ga upisivati
	}
	if (!parent->parent) //ako je koreni
		update_metadata();
}

bool EntryManager::does_exist(std::string s) {
	auto hold = get_entry(s);
	if (!hold)
		return false;
	return true;
}

EntryTree* EntryManager::get_entry(std::string s) {
	auto entries = split_filenames(s);
	if (!entries.size())
		return get_root_directory();
	EntryTree *iterate = root;
	for (auto name : entries) {
		iterate = iterate->get_entry(name);
		if (!iterate)
			return nullptr;
	}
	return iterate;
}
EntryFile* EntryManager::get_entry_file(std::string s) {
	EntryTree *hold = get_entry(s);
	if (!hold->is_directory())
		return reinterpret_cast<EntryFile*>(hold);
	else
		return nullptr; //exception
}
EntryDirectory* EntryManager::get_entry_directory(std::string s) {
	EntryTree *hold = get_entry(s);
	if (hold->is_directory())
		return reinterpret_cast<EntryDirectory*>(hold);
	else
		return nullptr; //exception
}
EntryDirectory* EntryManager::get_entry_parent(std::string s) {
	auto entries = split_filenames(s);
	if (!entries.size())
		exit(-1); //exception
	else if (entries.size() == 1)
		return get_root_directory();
	EntryTree *iterate = root;
	for (auto e : entries) {
		iterate = iterate->get_entry(e);
		if (!iterate)
			return nullptr;
	}
	return reinterpret_cast<EntryDirectory*>(iterate);
}

EntryDirectory* EntryManager::get_root_directory() {
	return reinterpret_cast<EntryDirectory*>(root);
}