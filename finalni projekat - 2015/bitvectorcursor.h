﻿#pragma once
#include <cstdint>
#include <utility>

class MetaManager;
using ClusterNo = unsigned long;
const uint32_t WordSizeInBits = sizeof(uint32_t) * CHAR_BIT;
class BitVectorCursor {
private:
	friend class MetaManager;

	uint32_t current_cluster, current_word, current_bit;
	MetaManager& manager;

	BitVectorCursor(MetaManager* manager);
	BitVectorCursor(MetaManager* manager, ClusterNo cluster_number);
	BitVectorCursor(MetaManager* manager, uint32_t current_cluster, uint32_t current_word, uint32_t current_bit);
	BitVectorCursor(const BitVectorCursor& cursor);
	BitVectorCursor & operator = (const BitVectorCursor & cursor);
	BitVectorCursor& operator ++ (); //sledeća reč
	bool operator < (const BitVectorCursor& cursor);

	uint32_t* get_word();
	void reset();
	ClusterNo to_cluster() const;
};

