#include "kernelfs.h"

KernelFS* KernelFS::singleton_fs = nullptr;

KernelFS::KernelFS(): partition_mutex() {
	free_partitions = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};
}

KernelFS* KernelFS::get_instance() {
	if (!singleton_fs)
		singleton_fs = new KernelFS(); //creates a singleton
	return singleton_fs;
}

KernelFS::~KernelFS() {
	if (singleton_fs)
		delete singleton_fs;
	singleton_fs = nullptr;
}

char KernelFS::mount(Partition *partition_object) {
	std::lock_guard<std::mutex> lock(partition_mutex);
	if (!free_partitions.size()) //if all partition letters are taken
		return 0; //error and unlock
	char partition_label = *free_partitions.begin(); //take the letter
	free_partitions.erase(partition_label); //remove it from the set of available letters
	specific_filesystems[partition_label] = new SpecificFilesystem(partition_label, partition_object);
	return partition_label;
}

char KernelFS::unmount(char partition) {
	std::unique_lock<std::mutex> lock(partition_mutex);
	if (free_partitions.count(partition)) //if the partition is not mounted
		return 0; //error and unlock
	while (specific_filesystems[partition]->number_of_open_files()) {
		specific_filesystems[partition]->stop_file_creation();
		lock.unlock();
		specific_filesystems[partition]->block();
		lock.lock();
	}
	delete specific_filesystems[partition];
	specific_filesystems.erase(partition);
	free_partitions.insert(partition); //add the letter to be mounted
	return 1; //success and unlock
}

char KernelFS::format(char partition) {
	std::unique_lock<std::mutex> lock(partition_mutex);
	if (free_partitions.count(partition)) //if the partition is not mounted
		return 0; //error and unlock
	while (specific_filesystems[partition]->number_of_open_files()) {
		specific_filesystems[partition]->stop_file_creation();
		lock.unlock();
		specific_filesystems[partition]->block();
		lock.lock();
	}
	specific_filesystems[partition]->format();
	return 1; //success and unlock
}

char KernelFS::read_root_dir(char part, EntryNum n, Directory & d) {
	std::lock_guard<std::mutex> lock(partition_mutex);
	if (!specific_filesystems.count(part))
		return 0;
	return specific_filesystems[part]->read_root_dir(n, d);
}

char KernelFS::does_exist(std::string file_name) {
	if (!is_valid_name(file_name))
		return 0;
	return specific_filesystems[file_name[0]]->does_exist(file_name);
}

File* KernelFS::open(std::string file_name, char mode) {
	if (!is_valid_name(file_name))
		return nullptr;
	if (!specific_filesystems[file_name[0]]->file_creation_allowed())
		return nullptr;
	File *f = nullptr;
	KernelFile* kf = nullptr;
	switch (mode) {
		case 'r': specific_filesystems[file_name[0]]->open_file_read(f = new File(), kf, file_name); break;
		case 'a': specific_filesystems[file_name[0]]->open_file_append(f = new File(), kf, file_name); break;
		case 'w': specific_filesystems[file_name[0]]->open_file_write(f = new File(), kf, file_name); break;
		default: exit(-1); //izuzetak
	}
	f->myImpl = kf;
	return f;
}

char KernelFS::delete_file(std::string file_name) {
	std::lock_guard<std::mutex> lock(partition_mutex);
	if (!is_valid_name(file_name))
		return 0;
	return specific_filesystems[file_name[0]]->delete_file(file_name);
}

char KernelFS::create_directory(std::string directory_name) {
	std::lock_guard<std::mutex> lock(partition_mutex);
	if (!is_valid_name(directory_name))
		return 0;
	return specific_filesystems[directory_name[0]]->create_directory(directory_name);
}

char KernelFS::delete_directory(std::string directory_name) {
	std::lock_guard<std::mutex> lock(partition_mutex);
	if (!is_valid_name(directory_name))
		return 0;
	return specific_filesystems[directory_name[0]]->delete_directory(directory_name);
}

char KernelFS::read_directory(std::string directory_name, EntryNum entry_number, Entry& entry) {
	std::lock_guard<std::mutex> lock(partition_mutex);
	if (!is_valid_name(directory_name))
		return 0;
	return specific_filesystems[directory_name[0]]->read_directory(directory_name, entry_number, entry);
}

bool KernelFS::is_valid_name(const std::string& name) const {
	return !name.empty() && std::isupper(name[0]) && name[1] == ':' && name[2] == '\\';
}
