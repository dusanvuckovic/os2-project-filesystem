﻿#include "metamanager.h"

MetaManager::MetaManager(SpecificFilesystem *file_system) : fs(*file_system), cursor(this) {
	num_of_words_in_last_cluster = (fs.get_number_of_all_clusters() % Helper::allocation_words_per_cluster);
	while (meta_clusters.size() != fs.number_of_meta_clusters) {
		meta_clusters.push_back(fs.clusters->get_cluster(meta_clusters.size(), false, false, false)); //broj klastera je simultano redni broj u kolekciji prvih N klastera
	}
	if (meta_clusters[0]->get_word(0) && 3) //javni test
		fs.is_public_test = true;
	word = cursor.get_word();
	if (!fs.is_public_test) {
		for (auto m : meta_clusters) {
			m->fill(); //napuni jedinicama
			allocate_next();
		}
		free_count = fs.number_of_all_clusters;
	}
	else {
		free_count = count_allocation();
		set_cursor_to_next_free();
		word = cursor.get_word();
		allocate_next(); //matični klaster
	}
}

uint32_t popcnt(uint32_t x) {
	x = x - ((x >> 1) & 0x55555555);
	x = (x & 0x33333333) + ((x >> 2) & 0x33333333);
	x = x + (x >> 8);
	x = x + (x >> 16);
	return x & 0x0000003F;
}

uint32_t MetaManager::count_allocation() {
	uint32_t count = 0;
	uint32_t limit = 0;
	uint32_t total_words = Helper::calculate_number_of_needed_structures(fs.get_number_of_all_clusters(), CHAR_BIT*sizeof(uint32_t));
	uint32_t badly_counted = fs.get_number_of_all_clusters() % (CHAR_BIT*sizeof(uint32_t));
	uint32_t* word = nullptr;
	for (auto c : meta_clusters) {
		limit = std::min(Helper::allocation_words_per_cluster, total_words);
		for (uint32_t i = 0; i < total_words; i++) {
			word = &c->get_word(i);
			count += __popcnt(*word);
		}
		total_words -= CHAR_BIT*sizeof(uint32_t);
	}
	for (uint32_t i = 0; i < badly_counted; i++)
		if (*word & (1 << (31 - i))) //ako su biti van granice jedinice, pogrešno ubrojani
			--count;
	return count;
}

MetaManager::~MetaManager() {
	for (auto c : meta_clusters) {
		fs.metadata->free_cluster(c->get_cluster_number());
		fs.clusters->return_cluster(c);
	}
	meta_clusters.clear();
}

void MetaManager::format() {
	for (auto c : meta_clusters)
		std::memset(c->get_data(), ~0, ClusterSize);
	free_count = fs.number_of_all_clusters;
	cursor.reset();
	for (uint32_t i = 0; i < meta_clusters.size(); i++)
		allocate_next();
}

void MetaManager::initialize() {
	format();
}

bool MetaManager::check_and_optionally_get_next() {
	if (*word)
		return true;
	++cursor;
	word = cursor.get_word();
	return false;
}

void MetaManager::set(BitVectorCursor & cursor) {
	*word &= ~(1 << (31 - cursor.current_bit));
}

void MetaManager::clear(BitVectorCursor & cursor) {
	meta_clusters[cursor.current_cluster]->get_word(cursor.current_word) |= 1 << (31 - cursor.current_bit);
}

ClusterNo MetaManager::allocate_next() {
	ClusterNo allocated = cursor.to_cluster();
	set(cursor);
	if (!--free_count)
		exit(-1);
	set_cursor_to_next_free();
	return allocated;
}

void MetaManager::set_cursor_to_next_free() {
	while (!check_and_optionally_get_next()); //dok je reč puna, ništa; inače sledeća + inkrementiranje kursora reči
	cursor.current_bit = __lzcnt(*word); //lzcnt->gledano sleva udesno
}

std::list<ClusterNo> MetaManager::allocate_next(uint32_t number) {
	std::list<ClusterNo> clusters;
	for (uint32_t i = 0; i < number; i++)
		clusters.push_back(allocate_next());
	return clusters;
}

void MetaManager::free_cluster(ClusterNo entry) {
	++free_count;
	BitVectorCursor freed(this, entry);
	clear(freed);
	if (++free_count == 1 || freed < cursor) { //ako je jedini ili je pre kursora
		cursor = freed; //vraćamo kursor nazad
		word = cursor.get_word();
	}
}

void MetaManager::free_clusters(std::vector<ClusterNo> numbers) {
	for (auto n : numbers)
		free_cluster(n);
}

uint32_t MetaManager::get_count_of_free_clusters() const {
	return free_count;
}

bool MetaManager::clusters_available() const {
	return free_count > 0;
}

bool MetaManager::clusters_available(uint32_t count) const {
	return free_count >= count;
}

bool MetaManager::is_last_word(uint32_t cluster_number, uint32_t word_number) const {
	if (is_last_cluster(cluster_number))
		return word_number == num_of_words_in_last_cluster;
	else
		return word_number == Helper::allocation_words_per_cluster;
}

bool MetaManager::is_last_cluster(uint32_t cluster_number) const {
	return cluster_number == meta_clusters.size();
}