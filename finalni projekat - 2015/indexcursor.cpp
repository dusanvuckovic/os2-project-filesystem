﻿#include "indexcursor.h"
#include "helper.h"

IndexCursor& IndexCursor::operator++() {
	++word;
	if ((word == Helper::allocation_words_per_cluster / 2 && !second_index) || word == Helper::allocation_words_per_cluster) {
		word = 0;
		if (++second_index == Helper::allocation_cluster_indices_per_index)
			exit(-1); //prekoračenje veličine indeksa
	}
	return *this;
}

IndexCursor & IndexCursor::operator--() {
	if (!word) {
		if (!second_index)
			exit(-1); //smanjen previše!
		--second_index;
		word = Helper::allocation_words_per_cluster - 1;
	}
	else
		--word;
	return *this;
}

bool IndexCursor::needs_second_level_index() const {
	return !word && second_index;
}

bool IndexCursor::needs_index_cluster() const {
	return !word && !second_index;
}
