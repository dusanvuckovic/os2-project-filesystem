#include "file.h"

File::File() {}

File::~File() {
	delete myImpl;
}

char File::write(BytesCnt bytes, char * buffer) {
	return myImpl->write(bytes, buffer);
}

BytesCnt File::read(BytesCnt bytes, char * buffer) {
	return myImpl->read(bytes, buffer);
}

char File::seek(BytesCnt bytes) {
	return myImpl->seek(bytes);
}

BytesCnt File::filePos() {
	return myImpl->file_position();
}

char File::eof() {
	return myImpl->eof();
}

BytesCnt File::getFileSize() {
	return myImpl->get_file_size();
}

char File::truncate() {
	return myImpl->truncate();
}