#pragma once

#include "cluster.h"
#include "fs.h"

class Cluster;
struct Entry;
using EntryNum = unsigned long;
class ClusterDirectory final: public Cluster {
public:
	ClusterDirectory(Partition*, const char, ClusterNo, bool);
	~ClusterDirectory() = default;

	void fill() override;
	uint32_t& get_word(EntryNum en) override;
	void get_entry(Entry&, EntryNum) override;
	void put_entry(Entry*, EntryNum) override;
	unsigned char* get_data() override;
	void put_data(uint32_t position, uint32_t size, std::string& data) override;
};

