#pragma once

#include "cluster.h"

using EntryNum = unsigned long;
class Cluster;
struct Entry;
class ClusterData: public Cluster {
public:
	ClusterData(Partition*, const char, ClusterNo, bool);
	~ClusterData() = default;
		
	void fill() override;
	uint32_t& get_word(EntryNum) override;
	void get_entry(Entry&, EntryNum) override;
	void put_entry(Entry*, EntryNum) override;
	unsigned char* get_data() override;
	void put_data(uint32_t position, uint32_t size, std::string& data) override;
};

