﻿#include "entrymanager.h"
#include "index.h"

EntryManager::EntryManager(SpecificFilesystem *filesystem): fs(*filesystem) {
	root = new EntryDirectory(fs.get_root_name(), nullptr, fs.get_number_of_meta_clusters(), 0);
	root->index.become_root_cluster(fs);
	reconstruct_fs(); //u najmanju ruku pravi root
}

EntryManager::~EntryManager() {
	clear_entries();
}

void EntryManager::format() {
	if (root) {
		root->index.deallocate(fs);
		delete root;
		fs.metadata->allocate_next();
	}
	root = new EntryDirectory(fs.get_root_name(), nullptr, fs.get_number_of_meta_clusters(), 0);
	root->index.become_root_cluster(fs);
	root->index.index_cluster->fill();
}

void EntryManager::clear_entries() {
	std::list<EntryTree*> entries = {root};
	while (entries.size()) {
		EntryTree* current = entries.front();
		entries.pop_front();
		if (current->is_directory())
			for (auto i: current->get_entries())
				entries.push_back(i);
		current->index.clear(fs);
		delete current;
	}
}

void EntryManager::reconstruct_fs() {
	std::list<EntryTree*> entries = { root };
	while (entries.size()) {
		EntryDirectory* current = reinterpret_cast<EntryDirectory*>(entries.front()); entries.pop_front();
		current->index.rebuild_index(fs); //pročitaj indeks i alociraj klastere
		current->branch_down(); //pročitaj ulaze i ubaci ih u stablo fajl-sistema
		for (auto inner_entry : current->get_entries())
			if (inner_entry->is_directory()) //isto za ostale direktorijume
				entries.push_back(inner_entry);
			else
				inner_entry->index.rebuild_index(fs); //inače, rb indeksa
	}
}

std::vector<std::string> EntryManager::split_filenames(std::string name) {
	std::vector<std::string> vector;
	name = name.substr(3); /*removes LTTR:\ */
	std::string hold;
	for (auto c : name) {
		if (c != '\\') //ako nije beksleš
			hold.push_back(c); //stavi u string
		else {
			vector.push_back(hold);
			hold.clear();
		}
	}
	if (hold != "")
		vector.push_back(hold);
	return vector;
}

bool EntryManager::check_parent_entry_availability(std::string& s) {
	auto entry = get_entry_parent(s);
	if (entry->get_size() != Helper::entry_entries_in_cluster)
		return true;
	return false;
}

void EntryManager::add_new_file(std::string s) {
	auto parent = get_entry_parent(s);
	s = s.substr(s.find_last_of('\\') + 1); //skraćuje string na ime fajla
	if (parent->is_full())
		parent->index.add_another_cluster(fs);
	parent->add_new_file(s);
}

void EntryManager::add_new_directory(std::string s) {
	auto parent = get_entry_parent(s);
	s = s.substr(s.find_last_of('\\')); //skraćuje string na ime fajla
	if (parent->is_full())
		parent->index.add_another_cluster(fs);
	parent->add_new_directory(s);
}

void EntryManager::remove_entry(std::string s) {
	auto names = split_filenames(s);
	auto entry = get_entry(s);
	auto parent = get_entry_parent(s);
	entry->index.deallocate(fs);
	parent->remove_entry(names.back());
	if (!(parent->size % Helper::entry_entries_in_cluster)) //ako je zauzeće pun klaster
		parent->index.remove_last_cluster(fs, true);
}

bool EntryManager::does_exist(std::string s) {
	auto hold = get_entry(s);
	if (!hold)
		return false;
	return true;
}

EntryTree* EntryManager::get_entry(std::string s) {
	auto entries = split_filenames(s);
	if (!entries.size())
		return get_root_directory();
	EntryTree *iterate = root;
	for (auto name : entries) {
		iterate = iterate->get_entry(name);
		if (!iterate)
			return nullptr;
	}
	return iterate;
}

EntryFile* EntryManager::get_entry_file(std::string s) {
	EntryTree *hold = get_entry(s);
	if (!hold->is_directory())
		return reinterpret_cast<EntryFile*>(hold);
	else
		return nullptr; //exception
}

EntryDirectory* EntryManager::get_entry_directory(std::string s) {
	EntryTree *hold = get_entry(s);
	if (hold->is_directory())
		return reinterpret_cast<EntryDirectory*>(hold);
	else
		return nullptr; //exception
}

EntryDirectory* EntryManager::get_entry_parent(std::string s) {
	auto entries = split_filenames(s);
	if (!entries.size())
		exit(-1); //exception
	else if (entries.size() == 1)
		return get_root_directory();
	EntryTree *iterate = root;
	for (auto e : entries) {
		iterate = iterate->get_entry(e);
		if (!iterate)
			return nullptr;
	}
	return reinterpret_cast<EntryDirectory*>(iterate);
}

EntryDirectory* EntryManager::get_root_directory() {
	return reinterpret_cast<EntryDirectory*>(root);
}