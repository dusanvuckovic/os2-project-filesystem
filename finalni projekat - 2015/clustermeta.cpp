﻿#include "clustermeta.h"

ClusterMeta::ClusterMeta(Partition* p, const char c, ClusterNo cn, bool b) : Cluster(p, c, cn, b) {
	if (b) //ako nije postojeći
		data.fill(1); //sve jedinice -> svi klasteri slobodni
	//prvo se puni nulama za osnovnu klasu pa onda jedinicama za nasleđenu
}

void ClusterMeta::fill() {
	std::memset(data.data(), ~0, ClusterSize);
	unconditional_write_to_partition();
}

uint32_t& ClusterMeta::get_word(EntryNum en) {
	if (en >= Helper::allocation_words_per_cluster)
		exit(-1);
	is_dirty = true;
	auto words = reinterpret_cast<uint32_t*>(data.data());
	return words[en];
}

void ClusterMeta::get_entry(Entry& e, EntryNum en) {
	std::exit(-1);
}

void ClusterMeta::put_entry(Entry* e, EntryNum en) {
	std::exit(-1);
}

unsigned char* ClusterMeta::get_data() {
	return data.data();
}

void ClusterMeta::put_data(uint32_t position, uint32_t size, std::string & data) {
	std::exit(-1);
}

