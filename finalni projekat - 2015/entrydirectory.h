﻿#pragma once
#include "entrytree.h"
#include "cluster.h"

class Cluster;
class EntryTree;
class EntryDirectory : public EntryTree {
private:
	std::map<std::string, std::pair<EntryTree*, uint32_t>> entry_position;
	void add_existing_entry(Entry& e, uint32_t position);
public:
	EntryDirectory(std::string name);
	EntryDirectory(std::string name, EntryTree* parent_entry);
	EntryDirectory(std::string name, EntryTree* parent, uint32_t index_cluster, uint32_t size);
	~EntryDirectory();

	void read_entry(Entry&e, uint32_t position);
	void write_entry(Entry* e, uint32_t position);
	void update_entry(EntryTree* child);

	bool is_full() const;
	void branch_down();

	inline uint32_t get_needed_cluster_size() const override;
	std::list<EntryTree*> get_entries() override;
	void add_new_file(std::string filename) override;
	void add_new_directory(std::string name) override;
	EntryTree* get_entry(std::string& s) override;
	void remove_entry(std::string& s) override;
	inline bool is_directory() const override;
};

