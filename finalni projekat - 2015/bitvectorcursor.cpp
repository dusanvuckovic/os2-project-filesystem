﻿#include "bitvectorcursor.h"
#include "fs.h"
#include "metamanager.h"
#include <climits>
#include <cstdint>

BitVectorCursor::BitVectorCursor(MetaManager* manager): manager(*manager), current_cluster(0), current_word(0), current_bit(0) {}

BitVectorCursor::BitVectorCursor(MetaManager* manager, ClusterNo cluster_number): manager(*manager) {
	current_cluster = cluster_number / ClusterSizeInBits;
	current_word = cluster_number / WordSizeInBits;
	current_bit = cluster_number % WordSizeInBits;
}

BitVectorCursor::BitVectorCursor(MetaManager* manager, uint32_t current_cluster, uint32_t current_word, uint32_t current_bit):
	manager(*manager), current_cluster(current_cluster), current_word(current_word), current_bit(current_bit) {}

BitVectorCursor& BitVectorCursor::operator=(const BitVectorCursor & cursor) {
	current_cluster = cursor.current_cluster;
	current_word = cursor.current_word;
	current_bit = cursor.current_bit;
	return *this;
}

BitVectorCursor& BitVectorCursor::operator++ () {
	if (manager.is_last_word(current_cluster, ++current_word)) {
		current_word = 0;
		if (manager.is_last_cluster(++current_cluster))
			current_cluster = 0;
	}
	return *this;
}

bool BitVectorCursor::operator<(const BitVectorCursor & cursor) {
	if (current_cluster < cursor.current_cluster)
		return true;
	else if (current_cluster > cursor.current_cluster)
		return false;
	if (current_word < cursor.current_word)
		return true;
	else if (current_word > cursor.current_word)
		return false;
	if (current_bit < cursor.current_bit)
		return true;
	else
		return false;
}

uint32_t * BitVectorCursor::get_word() {
	return &(manager.meta_clusters[current_cluster]->get_word(current_word));
}

void BitVectorCursor::reset() {
	current_cluster = current_word = current_bit = 0;
}

ClusterNo BitVectorCursor::to_cluster() const {
	return current_cluster * ClusterSizeInBits
		+ current_word * sizeof(uint32_t) * CHAR_BIT
		+ (current_bit) % 32;
}