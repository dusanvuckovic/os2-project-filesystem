﻿#pragma once

#include <iostream>
#include <map>
#include <list>

class Cluster;
class SpecificFilesystem;
class ClusterManager {
private:
	SpecificFilesystem& fs;
	std::map<ClusterNo, Cluster*> all_clusters;
	std::vector<uint32_t> counter;
public:
	ClusterManager(SpecificFilesystem*);
	~ClusterManager();

	Cluster* get_cluster(ClusterNo cluster_number, bool is_new, bool is_directory, bool is_index = false);
	std::list<Cluster*> get_multiple_clusters(std::list<ClusterNo> cluster_numbers, bool are_existing, bool are_directories);
	
	void return_cluster(Cluster* cluster);
	void return_multiple_clusters(std::vector<Cluster*>& clusters);
	void return_cluster_no_writeback(Cluster* cluster);
	void return_multiple_clusters_no_writeback(std::vector<Cluster*>& clusters);
};

