#include "clusterdata.h"

ClusterData::ClusterData(Partition* p, const char c, ClusterNo cn, bool b): Cluster(p, c, cn, b) {}

void ClusterData::fill() {
	exit(-1);
}

uint32_t& ClusterData::get_word(EntryNum en) {
	exit(-1);
}

void ClusterData::get_entry(Entry& e, EntryNum en) {
	std::exit(-1);
}

void ClusterData::put_entry(Entry* e, EntryNum en) {
	std::exit(-1);
}

unsigned char* ClusterData::get_data() {
	return data.data();
}

void ClusterData::put_data(uint32_t position, uint32_t size, std::string& data) {
	is_dirty = true;
	std::memcpy(this->data.data() + position, data.data(), size);
	data = data.substr(size);
}
