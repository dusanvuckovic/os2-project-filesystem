#pragma once

#include <vector>
#include <bitset>
#include <climits>
#include "specfs.h"
#include "bitvectorcursor.h"
#include "intrin.h"
const uint32_t ClusterSizeInBits = CHAR_BIT * ClusterSize;

class SpecificFilesystem;
class Cluster;
using ClusterNo = unsigned long;

uint32_t popcnt(uint32_t);

class MetaManager {
	friend class BitVectorCursor;
private:
	std::vector<Cluster*> meta_clusters;
	uint32_t count_allocation();
	void set_cursor_to_next_free();

	BitVectorCursor cursor;
	uint32_t *word; //last_accessed_word
	bool check_and_optionally_get_next();
	void set(BitVectorCursor& cursor);
	void clear(BitVectorCursor& cursor);
	uint32_t num_of_words_in_last_cluster;

public:
	MetaManager(SpecificFilesystem*);
	~MetaManager();
	SpecificFilesystem& fs;

	void format();
	void initialize();

	bool is_last_word(uint32_t cluster_number, uint32_t word_number) const;
	bool is_last_cluster(uint32_t cluster_number) const;

	uint32_t free_count;

	bool clusters_available() const;
	bool clusters_available(uint32_t) const;
	ClusterNo allocate_next();
	std::list<ClusterNo> allocate_next(uint32_t number);
	void free_cluster(ClusterNo to_be_freed);
	void free_clusters(std::vector<ClusterNo> to_be_freed);
	uint32_t get_count_of_free_clusters() const;
};