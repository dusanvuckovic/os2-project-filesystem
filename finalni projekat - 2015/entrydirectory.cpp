﻿#include "entrydirectory.h"
#include "helper.h"
#include "fs.h"

EntryDirectory::EntryDirectory(std::string name) : EntryTree(true, name) {} //prazan
EntryDirectory::EntryDirectory(std::string name, EntryTree* parent_entry) : EntryTree(true, name, parent_entry) {} //prazan
EntryDirectory::EntryDirectory(std::string name, EntryTree* parent, uint32_t index_cluster, uint32_t size) : EntryTree(true, name, parent, index_cluster, size) {} //postojeći
EntryDirectory::~EntryDirectory() = default; //klasteri se dealociraju ručno

void EntryDirectory::read_entry(Entry& e, uint32_t position) {
	index.read_entry(e, position);
}

void EntryDirectory::write_entry(Entry* e, uint32_t position) {
	index.write_entry(e, position);
}

std::list<EntryTree*> EntryDirectory::get_entries() {
	std::list<EntryTree*> entries;
	for (auto c : entry_position)
		entries.push_back(c.second.first);
	return entries;
}
void EntryDirectory::add_existing_entry(Entry& e, uint32_t position) {
	EntryTree* child = nullptr;
	if (!e.reserved || e.reserved == 1)
		child = new EntryFile(Helper::get_string_from_entry(&e), parent, e.indexCluster, e.size);
	else
		child = new EntryDirectory(Helper::get_string_from_entry(&e), parent, e.indexCluster, e.size);
	entry_position[Helper::get_string_from_entry(&e)].first = child;
	entry_position[Helper::get_string_from_entry(&e)].second = position;
}

void EntryDirectory::add_new_file(std::string filename) {
	entry_position[filename].first = new EntryFile(filename, this);
	entry_position[filename].second = size;
	Entry e;
	Helper::fill_entry_file(e, filename);
	write_entry(&e, size);
	++size;
}

void EntryDirectory::add_new_directory(std::string name) {
	entry_position[name].first = new EntryDirectory(name, this);
	entry_position[name].second = size;
	Entry e;
	Helper::fill_entry_directory(e, name);
	write_entry(&e, size);
	++size;
}

EntryTree* EntryDirectory::get_entry(std::string& s) {
	if (!entry_position.count(s))
		return nullptr;
	return entry_position[s].first;
}

void EntryDirectory::remove_entry(std::string& name) {
	if (!entry_position.count(name))
		return;
	auto rem_entry_position = entry_position[name];
	if (rem_entry_position.second != entry_position.size() - 1) { //ako nije poslednja, prekopiraj poslednju na novooslobođenu poziciju
		Entry e;
		read_entry(e, entry_position.size() - 1);
		write_entry(&e, rem_entry_position.second); //prekopiraj sam ulaz
		auto moved_entry_name = Helper::get_string_from_entry(&e);
		entry_position[moved_entry_name].second = rem_entry_position.second; //novi ulaz sad ukazuje na stariju poziciju
	}
	Entry null_entry{ 0, 0, 0, 0, 0 };
	write_entry(&null_entry, entry_position.size() - 1); //prekopiraj nulti ulaz u memoriju
	entry_position.erase(name);
	--size;
	delete rem_entry_position.first;
}

bool EntryDirectory::is_directory() const {
	return true;
}

bool EntryDirectory::is_full() const {
	return !(size % Helper::entry_entries_in_cluster);
}

uint32_t EntryDirectory::get_needed_cluster_size() const {
	return Helper::calculate_number_of_needed_structures(size, Helper::entry_entries_in_cluster);
}

void EntryDirectory::branch_down() {
	Entry e;
	for (uint32_t i = 0; i < size; i++) {
		read_entry(e, i);
		add_existing_entry(e, i);
	}
}

void EntryDirectory::update_entry(EntryTree* child) {
	if (!child)
		return;
	std::string child_name = child->get_name();
	if (!entry_position.count(child_name))
		return;
	Entry e;
	uint32_t child_position = entry_position[child_name].second;
	read_entry(e, child_position);
	e.indexCluster = child->get_index_cluster();
	e.size = child->get_size();
	write_entry(&e, child_position);
}