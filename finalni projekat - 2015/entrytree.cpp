#include "entrytree.h"
#include "entrydirectory.h"
#include "helper.h"

EntryTree::EntryTree(bool is_directory, std::string name) : index(is_directory), name(name), 
index_cluster(index.index_cluster_number), size(index.size), parent(nullptr) {}
EntryTree::EntryTree(bool is_directory, std::string name, EntryTree* parent): index(is_directory), name(name), 
index_cluster(index.index_cluster_number), size(index.size), parent(parent) {}
EntryTree::EntryTree(bool is_directory, std::string name, EntryTree* parent, uint32_t index_cluster, uint32_t size): 
	index(is_directory, index_cluster), name(name), parent(parent), index_cluster(index.index_cluster_number), size(index.size) {
	this->size = size;
}

uint32_t EntryTree::get_size() const {
	return size;
}

std::string EntryTree::get_name() const {
	return name;
}

ClusterNo EntryTree::get_index_cluster() const {
	return index_cluster;
}

void EntryTree::update_parent() {
	if (!parent)
		return;
	reinterpret_cast<EntryDirectory*>(parent)->update_entry(this);
}