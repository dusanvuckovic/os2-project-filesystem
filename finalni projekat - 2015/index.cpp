﻿#include "index.h"
#include "specfs.h"
#include "helper.h"

Index::Index(bool directory_index, ClusterNo index_cluster_number) : index_cluster_number(index_cluster_number),
directory_index(directory_index) {}

Index::~Index() {}

void Index::clear(SpecificFilesystem& fs) {
	for (auto data_cluster : data_clusters)
		fs.clusters->return_cluster(data_cluster); //očisti sve klastere sa podacima
	for (auto second_index_cluster: indirect_index_clusters)
		fs.clusters->return_cluster(second_index_cluster);
	if (index_cluster)
		fs.clusters->return_cluster(index_cluster);
}

void Index::deallocate(SpecificFilesystem& fs) {
	for (auto data_cluster : data_clusters) {
		fs.metadata->free_cluster(data_cluster->get_cluster_number());
		fs.clusters->return_cluster(data_cluster);
	}
	for (auto second_index_cluster : indirect_index_clusters) {
		fs.metadata->free_cluster(second_index_cluster->get_cluster_number());
		fs.clusters->return_cluster(second_index_cluster);
		}
	if (index_cluster) {
		fs.metadata->free_cluster(index_cluster->get_cluster_number());
		fs.clusters->return_cluster(index_cluster);
	}
}

void Index::rebuild_index(SpecificFilesystem& fs) {
	if (!root_index)
		add_first_level_index(fs);
	bool rebuilding_done = process_first_level_index(fs);
	if (rebuilding_done) {
		if (root_index)
			size = determine_size();
		return;
	}
	add_second_level_indices(fs);
	process_second_level_indices(fs);

	if (root_index)
		size = determine_size();
}

Cluster* Index::select_index_cluster() const {
	if (!cursor.second_index)
		return index_cluster;
	return indirect_index_clusters[cursor.second_index - 1];
}

void Index::become_root_cluster(SpecificFilesystem & fs) {
	root_index = true;
	add_first_level_index(fs);
}

inline void Index::add_first_level_index(SpecificFilesystem& fs) {
	bool is_new = false;
	if (!index_cluster_number) {
		index_cluster_number = fs.metadata->allocate_next();
		is_new = true;
	}
	index_cluster = fs.clusters->get_cluster(index_cluster_number, is_new, false, true);
}

inline void Index::remove_first_level_index(SpecificFilesystem& fs) {
	fs.metadata->free_cluster(index_cluster->get_cluster_number());
	fs.clusters->return_cluster(index_cluster);
	index_cluster = nullptr;
	index_cluster_number = 0;
}

bool Index::process_first_level_index(SpecificFilesystem& fs) {
	uint32_t word = 0;
	for (uint32_t i = 0; i < single_level_clusters; i++) {
		word = index_cluster->get_word(i);
		if (!word)
			return true;
		data_clusters.push_back(fs.clusters->get_cluster(word, false, directory_index, false));
		++cursor;
	}
	return false;
}

inline void Index::add_next_second_level_index(SpecificFilesystem& fs) {
	uint32_t position = cursor.second_index - 1, cluster_number = 0;
	indirect_index_clusters.push_back(fs.clusters->get_cluster(cluster_number = fs.metadata->allocate_next(), true, false, true)); //ubaci objekat klastera
	index_cluster->get_word(single_level_clusters + position) = cluster_number; //ubaci podatak o indeksu drugog u indeks prvog nivoa
}

inline void Index::remove_last_second_level_index(SpecificFilesystem& fs) {
	uint32_t position = cursor.second_index;
	auto index_clust = indirect_index_clusters[position]; //uzmi klaster
	indirect_index_clusters[position] = nullptr;
	index_cluster->get_word(single_level_clusters + position) = 0; //ubaci nulu u indeks prvog nivoa
	fs.metadata->free_cluster(index_clust->get_cluster_number()); //oslobodi klaster bit-vektor
	fs.clusters->return_cluster_no_writeback(index_clust); //oslobodi klaster u listi klastera
}

void Index::add_second_level_indices(SpecificFilesystem &fs) {
	for (uint32_t i = single_level_clusters; i < all_indexing_clusters; i++) {
		uint32_t word = index_cluster->get_word(i);
		if (!word)
			break; //kraj broja ulaza
		indirect_index_clusters.push_back(fs.clusters->get_cluster(word, false, false, true));
	}
}

void Index::process_second_level_indices(SpecificFilesystem &fs) {
	for (auto c : indirect_index_clusters) { //za sve klastere drugog nivoa
		for (uint32_t i = 0; i < all_indexing_clusters; i++) {
			uint32_t word = c->get_word(i);
			if (!word)
				return;
			data_clusters.push_back(fs.clusters->get_cluster(word, false, directory_index, false));
			++cursor;
		}
	}
}

void Index::read_entry(Entry& e, uint32_t position) {
	data_clusters[position / Helper::entry_entries_in_cluster]->get_entry(e, position % Helper::entry_entries_in_cluster);
}

void Index::write_entry(Entry* e, uint32_t position) {
	data_clusters[position / Helper::entry_entries_in_cluster]->put_entry(e, position % Helper::entry_entries_in_cluster);
}

uint32_t Index::determine_size() const {
	uint32_t size = 0;
	Entry e;
	for (auto c : data_clusters) {
		for (uint32_t i = 0; i < Helper::allocation_entries_per_cluster; i++) {
			c->get_entry(e, i);
			if (!e.name[0])
				return size;
			else
				++size;
		}
	}
	return size;
}

uint32_t Index::get_current_cluster_size() const {
	return data_clusters.size();
}

void Index::add_another_cluster(SpecificFilesystem &fs) {
	if (!root_index && cursor.needs_index_cluster()) //ako nije koreni indeks i nema alociran indeksni klaster
		add_first_level_index(fs);
	if (cursor.needs_second_level_index()) //ako je reč 0 i postoji indeks
		add_next_second_level_index(fs);
	data_clusters.push_back(fs.clusters->get_cluster(fs.metadata->allocate_next(), false, directory_index, false));
	select_index_cluster()->get_word(cursor.word) = data_clusters.back()->get_cluster_number();
	++cursor;
}

void Index::remove_last_cluster(SpecificFilesystem &fs, bool no_writeback) {
	auto removed = data_clusters.back();
	data_clusters.pop_back();
	--cursor; //sada pokazuje na prethodno držanu lokaciju
	select_index_cluster()->get_word(cursor.word) = 0;
	if (cursor.needs_second_level_index()) //ako je prvo na šta pokazujemo neki indeks, više nam ne treba
		remove_last_second_level_index(fs); //pa ga pomeramo
	else if (!root_index && cursor.needs_index_cluster())
		remove_first_level_index(fs);
	fs.metadata->free_cluster(removed->get_cluster_number());
	if (no_writeback)
		fs.clusters->return_cluster_no_writeback(removed);
	else
		fs.clusters->return_cluster(removed);
}

void Index::truncate_clusters(SpecificFilesystem& fs, uint32_t starting_position) {
	auto total_cluster_number = get_current_cluster_size();
	auto to_clear = total_cluster_number - starting_position;
	for (uint32_t i = 0; i < to_clear; i++)
		remove_last_cluster(fs, true);
	data_clusters.resize(total_cluster_number); //ostaju prvih n klastera obuhvacena kursorom
}

void Index::add_additional_clusters(SpecificFilesystem& fs, uint32_t number_of_new_clusters) {
 	for (uint32_t i = 0; i < number_of_new_clusters; i++)
		add_another_cluster(fs);
}