#include "clusterdirectory.h"

ClusterDirectory::ClusterDirectory(Partition* p, const char c, ClusterNo cn, bool b) : Cluster(p, c, cn, b) {}

void ClusterDirectory::fill() {
	exit(-1);
}

uint32_t& ClusterDirectory::get_word(EntryNum en) {
	exit(-1);
}

void ClusterDirectory::get_entry(Entry& e, EntryNum n) {
	auto entries = reinterpret_cast<Entry*>(data.data());
	e = entries[n];
}

void ClusterDirectory::put_entry(Entry* e, EntryNum n) {
	is_dirty = true;
	auto entries = reinterpret_cast<Entry*>(data.data());
	entries[n] = *e;
}

unsigned char* ClusterDirectory::get_data() {
	std::exit(-1);
}

void ClusterDirectory::put_data(uint32_t position, uint32_t size, std::string & data) {
	std::exit(-1);
}