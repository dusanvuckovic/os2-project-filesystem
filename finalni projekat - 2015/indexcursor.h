#pragma once
#include <cstdint>
#include "part.h"

class IndexCursor {
	friend class Index;
private:
	uint32_t word;
	uint32_t second_index;
public:

	IndexCursor& operator ++ ();
	IndexCursor& operator -- ();

	bool needs_second_level_index() const;
	bool needs_index_cluster() const;

	IndexCursor() = default;
	~IndexCursor() = default;
};

