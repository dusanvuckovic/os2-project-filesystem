﻿// File: fs.h

#pragma once
#include "kernelfs.h"

typedef unsigned long BytesCnt;
typedef unsigned long EntryNum;

const unsigned long ENTRYCNT = 64;
const unsigned int FNAMELEN = 8;
const unsigned int FEXTLEN = 3;
struct Entry {
	char name[FNAMELEN];
	char ext[FEXTLEN];
	char reserved;
	unsigned long indexCluster;
	unsigned long size;
};

typedef Entry Directory[ENTRYCNT];

class KernelFS;
class Partition;
class File;
class FS {
public:
	~FS();
	static char mount(Partition* partition); //montira particiju; vraca dodeljeno slovo
	static char unmount(char part); //demontira particiju oznacenu datim slovom; vraca 0 u slucaju neuspeha ili 1 u slucaju uspeha
	static char format(char part); //particija zadata slovom se formatira; vraca 0 u slucaju neuspeha ili 1 u slucaju uspeha
	static char readRootDir(char part, EntryNum n, Directory &d); //prvim argumentom se zadaje particija, drugim redni broj validnog ulaza od kog se počinje čitanje
	static char doesExist(char* fname); //argument je naziv fajla sa apsolutnom putanjom
	static File* open(char* fname, char mode); //prvi argument je naziv fajla zadat apsolutnom putanjom, drugi modalitet
	static char deleteFile(char* fname); // argument je naziv fajla zadat apsolutnom putanjom

	//deprecated
	static char createDir(char* dirname); //argument je naziv direktorijuma zadat apsolutnom putanjom
	static char deleteDir(char* dirname); //argument je naziv direktorijuma zadat apsolutnom putanjom
	static char readDir(char* dirname, EntryNum n, Entry &e); //prvim argumentom se zadaje apsolutna putanja, drugim redni broj ulaza koji se cita, treci argument je adresa na kojoj se smesta procitani ulaz
	//deprecated
protected:
	FS();
	static KernelFS *myImpl;
};