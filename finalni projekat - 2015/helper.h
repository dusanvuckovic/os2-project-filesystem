#pragma once

#include <cstdint>
#include <string>
#include "part.h"

struct Entry;
class Helper {
public:
	Helper() = delete;
	~Helper() = delete;
	
	static uint32_t calculate_number_of_needed_structures(uint32_t number_of_entries, uint32_t entries_per_cluster);
	static uint32_t Helper::calculate_number_of_meta_clusters(uint32_t total_cluster_entries);
	static void fill_entry_file(Entry& e, std::string& s, uint32_t index_cluster = 0, uint32_t size = 0);
	static void fill_entry_directory(Entry& e, std::string s, uint32_t index_cluster = 0, uint32_t size = 0);
	static std::string get_string_from_entry(Entry *e);

	static const uint32_t entry_entries_in_cluster = ClusterSize / 20;
	static const uint32_t allocation_entries_per_cluster = ClusterSize;
	static const uint32_t allocation_bits_per_cluster = ClusterSize * CHAR_BIT;
	static const uint32_t allocation_words_per_cluster = ClusterSize / sizeof(uint32_t);
	static const uint32_t allocation_cluster_indices_per_index = ClusterSize / sizeof(ClusterNo) / 2;
	static const uint32_t allocation_cluster_numbers_per_cluster = ClusterSize / sizeof(ClusterNo); 
};

