#pragma once

#include <algorithm>
#include <vector>
#include "kernelfs.h"
#include "kernelfile.h"
#include "entrytree.h"
#include "helper.h"
#include "clustermanager.h"
#include "metamanager.h"
#include "entrymanager.h"
#include "filemanager.h"
#include "conditionvariablefifo.h"

const unsigned long ENTRYCNT_KFS = 64;

using EntryNum = unsigned long;
struct Entry;
using Directory = Entry[ENTRYCNT_KFS];
class Cluster;
class File;
class KernelFile;
class ConditionVariableSingle;

class SpecificFilesystem {
	friend class Index;
	friend class KernelFile;
	friend class MetaManager;
	friend class ClusterManager;
	friend class EntryManager;
	friend class FileManager;

private:
	const char partition_label;
	Partition* partition_object;

	bool is_public_test;

	std::mutex partition_mutex;
	std::mutex open_mutex;
	ConditionVariableFIFOC11 synchro;

	uint32_t number_of_meta_clusters;
	uint32_t number_of_all_clusters;

	ClusterManager* clusters;
	MetaManager* metadata;
	EntryManager* entries;
	FileManager* files;

public:
	//interface
	SpecificFilesystem(const char, Partition*); //mounting
	~SpecificFilesystem(); //dismounting
	void format();
	char read_root_dir(EntryNum n, Directory& d);
	bool does_exist(std::string&);
	void open_file_read(File*, KernelFile*&, std::string);
	void open_file_append(File*, KernelFile*&, std::string);
	void open_file_write(File*, KernelFile*&, std::string);
	char delete_file(std::string&);
	//interface

	//additional
	void block();
	void unblock();
	uint32_t number_of_open_files() const;
	void stop_file_creation();
	bool file_creation_allowed() const;
	std::string get_root_name() const;
	uint32_t get_number_of_all_clusters() const;
	uint32_t get_number_of_meta_clusters() const;
	//additional

	//deprecated
	char create_directory(std::string&);
	char delete_directory(std::string&);
	char read_directory(std::string&, EntryNum, Entry&);
	//deprecated
};