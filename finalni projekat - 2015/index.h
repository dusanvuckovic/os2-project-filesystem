﻿#pragma once
#include <cstdint>
#include <array>
#include <vector>
#include <list>
#include "part.h"
#include "cluster.h"
#include "helper.h"
#include "indexcursor.h"
using ClusterNo = unsigned long;

const uint32_t INDEXSIZE = ClusterSize / sizeof(ClusterNo);
class SpecificFilesystem;

const uint32_t single_level_clusters = INDEXSIZE / 2;
const uint32_t all_indexing_clusters = INDEXSIZE;
const uint32_t max_size_in_clusters = (INDEXSIZE / 2) + (INDEXSIZE / 2) * Helper::allocation_cluster_numbers_per_cluster;
const uint32_t max_size_in_bytes = ClusterSize * max_size_in_clusters;

class Index {
	friend class EntryTree;
	friend class EntryDirectory;
	friend class EntryFile;
	friend class EntryManager;
	friend class KernelFile;
private:
	bool root_index; //jesam li indeks korenog direktorijuma?
	bool directory_index; //indeksiram li ulaze ili fizičke podatke?

	uint32_t size; //fajla ili broj ulaza
	uint32_t index_cluster_number;
	Cluster* index_cluster;
	std::vector<Cluster*> indirect_index_clusters; //najviše 256
	std::vector<Cluster*> data_clusters; //najviše ~130k

	IndexCursor cursor;

	Cluster* select_index_cluster() const;
	void become_root_cluster(SpecificFilesystem& fs);
	void add_first_level_index(SpecificFilesystem&);
	void remove_first_level_index(SpecificFilesystem&);
	bool process_first_level_index(SpecificFilesystem&);
	void add_next_second_level_index(SpecificFilesystem&);
	void remove_last_second_level_index(SpecificFilesystem&);
	void add_second_level_indices(SpecificFilesystem&);
	void process_second_level_indices(SpecificFilesystem&);
public:
	Index(bool is_directory, ClusterNo index_cluster_number = 0);
	~Index();


	void clear(SpecificFilesystem& fs);
	void deallocate(SpecificFilesystem & fs);
	void rebuild_index(SpecificFilesystem& fs);

	void read_entry(Entry& e, uint32_t position);
	void write_entry(Entry* e, uint32_t position);

	uint32_t determine_size() const;
	uint32_t get_current_cluster_size() const;
	void add_another_cluster(SpecificFilesystem &fs);
	void add_additional_clusters(SpecificFilesystem &fs, uint32_t number_of_new_clusters);
	void remove_last_cluster(SpecificFilesystem &fs, bool no_writeback = false);
	void truncate_clusters(SpecificFilesystem &fs, uint32_t position);
};

