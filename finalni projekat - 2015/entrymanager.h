#pragma once

#include <string>
#include <vector>
#include "fs.h"
#include "entrytree.h"
#include "entryfile.h"
#include "entrydirectory.h"

using EntryNum = unsigned long;
class SpecificFilesystem;
class EntryFile;
class EntryDirectory;
class EntryManager {
private:
	SpecificFilesystem& fs;
	EntryTree *root;
	std::vector<std::string> split_filenames(std::string);
	void reconstruct_fs();
public:
	EntryManager(SpecificFilesystem*);
	~EntryManager();
	void format();
	void clear_entries();

	bool does_exist(std::string);
	EntryTree* get_entry(std::string);
	EntryFile* get_entry_file(std::string);
	EntryDirectory* get_entry_directory(std::string);
	EntryDirectory* get_entry_parent(std::string);
	EntryDirectory* get_root_directory();
		
	bool check_parent_entry_availability(std::string&);

	void add_new_file(std::string);
	void add_new_directory(std::string);

	void remove_entry(std::string);
};

