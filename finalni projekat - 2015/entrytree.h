﻿#pragma once

#include <cstdint>
#include <map>
#include <string>
#include <list>
#include <vector>
#include "index.h"

using ClusterNo = unsigned long;
class Cluster;
class EntryTree {
protected:
	friend class EntryManager;

	uint32_t& index_cluster;
	uint32_t& size;
	EntryTree* parent;
	std::string name;
	Index index;
public:
	EntryTree(bool is_directory, std::string name);
	EntryTree(bool is_directory, std::string name, EntryTree* parent);
	EntryTree(bool is_directory, std::string name, EntryTree* parent, uint32_t index_cluster, uint32_t size);
	virtual ~EntryTree() = default;
	EntryTree(EntryTree&) = delete;
	EntryTree(EntryTree&&) = delete;

	void update_parent();

	uint32_t get_size() const;
	ClusterNo get_index_cluster() const;
	std::string get_name() const;

	virtual uint32_t get_needed_cluster_size() const = 0;
	
	virtual std::list<EntryTree*> get_entries() = 0;
	virtual void add_new_file(std::string name) = 0;
	virtual void add_new_directory(std::string name) = 0;
	virtual EntryTree* get_entry(std::string& s) = 0;
	virtual void remove_entry(std::string& s) = 0;
	virtual bool is_directory() const = 0;
};
