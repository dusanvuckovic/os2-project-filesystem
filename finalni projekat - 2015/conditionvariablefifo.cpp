#include "conditionvariablefifo.h"
#include <memory>
#include <condition_variable>

void ConditionVariableFIFOC11::wait() {
	std::unique_lock<std::mutex> acquire(blocker_lock); //locks the mutex
	std::condition_variable temp_variable; //creates a variable
	blocker_variables.push_back(&temp_variable); //adds it to be unblocked later
	temp_variable.wait(acquire); //here the thread blocks
}

void ConditionVariableFIFOC11::signal() {
	std::unique_lock<std::mutex> acquire(blocker_lock); //locks the mutex
	if (!blocker_variables.size()) //if there are no threads waiting
		return;
	blocker_variables.front()->notify_one(); //else wake up the previously waiting thread
	blocker_variables.pop_front(); //and remove it from the queue (the cv will be automatically deleted after the thread exits the block function)
}

void ConditionVariableFIFOC11::signal_all() {
	std::unique_lock<std::mutex> acquire(blocker_lock); //locks the mutex
	while (blocker_variables.size()) {
		blocker_variables.front()->notify_one();
		blocker_variables.pop_front(); //and remove it from the queue (the cv will be automatically deleted after the thread exits the block function)
	}
}
uint32_t ConditionVariableFIFOC11::size() const {
	return blocker_variables.size();
}

bool ConditionVariableFIFOC11::is_empty() const {
	if (blocker_variables.size())
		return false;
	return true;
}