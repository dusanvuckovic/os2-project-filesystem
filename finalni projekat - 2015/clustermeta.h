#pragma once

#include "cluster.h"
#include "kernelfs.h"

class Cluster;
class Partition;
struct Metadata;
class ClusterMeta final: public Cluster {
public:
	ClusterMeta(Partition*, const char, ClusterNo, bool);
	~ClusterMeta() = default;

	void fill() override;
	uint32_t& get_word(EntryNum en) override;
	void get_entry(Entry&, EntryNum) override;
	void put_entry(Entry*, EntryNum) override;
	unsigned char* get_data() override;
	void put_data(uint32_t position, uint32_t size, std::string& data) override;
};

