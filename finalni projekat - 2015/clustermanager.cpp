#include "cluster.h"
#include "clustermanager.h"
#include "clusterdata.h"
#include "clusterdirectory.h"
#include "clustermeta.h"
#include "clusterindex.h"
#include <cstdlib>

ClusterManager::ClusterManager(SpecificFilesystem *file_system): fs(*file_system) {
	counter.assign(fs.number_of_all_clusters, 0);
}

ClusterManager::~ClusterManager() {}

Cluster* ClusterManager::get_cluster(ClusterNo cluster_number, bool is_new, bool is_directory, bool is_index) {
	Cluster *candidate = nullptr;
	if (all_clusters.count(cluster_number)) {
		++counter[cluster_number];
		return all_clusters[cluster_number];
	}
	if (cluster_number < fs.number_of_meta_clusters)
		candidate = new ClusterMeta(fs.partition_object, fs.partition_label, cluster_number, is_new);
	else if (is_directory)
		candidate = new ClusterDirectory(fs.partition_object, fs.partition_label, cluster_number, is_new);
	else if (is_index)
		candidate = new ClusterIndex(fs.partition_object, fs.partition_label, cluster_number, is_new);
	else
		candidate = new ClusterData(fs.partition_object, fs.partition_label, cluster_number, is_new);
	all_clusters[cluster_number] = candidate;
	++counter[cluster_number];
	return candidate;
}

std::list<Cluster*> ClusterManager::get_multiple_clusters(std::list<ClusterNo> cluster_numbers, bool are_existing, bool are_directories) {
	std::list<Cluster*> clusters;
	while (cluster_numbers.size()) {
		clusters.push_back(get_cluster(cluster_numbers.front(), are_existing, are_directories));
		cluster_numbers.pop_front();
	}
	return clusters;
}

void ClusterManager::return_cluster(Cluster* c) {
	if (!(--counter[c->cluster_number])) {
		all_clusters.erase(c->cluster_number);
		delete c;
	}
}

void ClusterManager::return_multiple_clusters(std::vector<Cluster*>& clusters) {
	for (auto c: clusters)
		return_cluster(c);
}

void ClusterManager::return_cluster_no_writeback(Cluster *c) {
	c->is_dirty = false;
	return_cluster(c);
}

void ClusterManager::return_multiple_clusters_no_writeback(std::vector<Cluster*>& clusters) {
	for (auto c : clusters)
		return_cluster_no_writeback(c);
}