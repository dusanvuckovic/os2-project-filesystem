#include "clusterindex.h"
#include "helper.h"

ClusterIndex::ClusterIndex(Partition * p, const char c, ClusterNo cn, bool b) : Cluster(p, c, cn, b) {}

void ClusterIndex::fill() {
	std::memset(data.data(), 0, ClusterSize);
	unconditional_write_to_partition();
}

uint32_t & ClusterIndex::get_word(EntryNum en) {
	if (en >= Helper::allocation_words_per_cluster)
		exit(-1);
	is_dirty = true;
	auto words = reinterpret_cast<uint32_t*>(data.data());
	return words[en];
}

void ClusterIndex::get_entry(Entry& e, EntryNum en) {
	exit(-1);
}

void ClusterIndex::put_entry(Entry* e, EntryNum en) {
	exit(-1);
}

unsigned char* ClusterIndex::get_data() {
	exit(-1);
}

void ClusterIndex::put_data(uint32_t position, uint32_t size, std::string & data) {
	exit(-1);
}
