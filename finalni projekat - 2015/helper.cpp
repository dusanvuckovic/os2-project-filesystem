#include "helper.h"
#include "fs.h"

uint32_t Helper::calculate_number_of_needed_structures(uint32_t number_of_entries, uint32_t entries_per_cluster) {
	return (!(number_of_entries % entries_per_cluster)) ? number_of_entries / entries_per_cluster : (number_of_entries / entries_per_cluster) + 1;
}

uint32_t Helper::calculate_number_of_meta_clusters(uint32_t total_cluster_entries) {
	return calculate_number_of_needed_structures(total_cluster_entries, allocation_bits_per_cluster);
}

void Helper::fill_entry_file(Entry& e, std::string& s, uint32_t index_cluster, uint32_t size) { //index_cluster, size default to zero
	auto delimiter = s.find(".");
	auto filename = s.substr(0, delimiter);
	auto extension = s.substr(delimiter + 1);
	while (filename.size() != FNAMELEN)
		filename += " ";
	while (extension.size() != FEXTLEN)
		extension += " ";
	e.reserved = 0;
	std::memcpy(e.name, filename.data(), FNAMELEN);
	std::memcpy(e.ext, extension.data(), FEXTLEN);
	e.indexCluster = index_cluster;
	e.size = size;
	return;
}

void Helper::fill_entry_directory(Entry& e, std::string filename, uint32_t index_cluster, uint32_t size) { //index_cluster, size default to zero
	while (filename.size() != FNAMELEN)
		filename += " ";
	e.reserved = 2;
	std::memcpy(e.name, filename.data(), FEXTLEN);
	std::memcpy(e.ext, "   ", FEXTLEN);
	e.indexCluster = index_cluster;
	e.size = size;
	return;
}

std::string Helper::get_string_from_entry(Entry *e) {
	std::string hold = e->name;
	hold.erase(FNAMELEN);
	if (!e->reserved || e->reserved == 1) {
		hold += '.';
		hold += e->ext;
		hold.erase(FNAMELEN+FEXTLEN+1);
		std::string helper;
		for (auto c : hold)
			if (c != ' ')
				helper.push_back(c);
		hold = helper;
	}
	else
		hold.erase(hold.find(' '));
	return hold;
}
